<?php

namespace Jcms\Core\Ext;

class Paginacao {
    
    public $total_paginas;
    public $pagina_atual;
    public $todas_paginas = true;
    public $limiar;
    public $primeira = true;
    public $anterior = false;
    public $proxima = false;
    public $ultima = true;
    public $str_primeira = null;
    public $str_anterior = null;
    public $str_proxima = null;        
    public $str_ultima = null;
    public $max_paginas;
    public $base_url;
    public $query_string;
    public $item_spacer = null;        
    private $paginacao="";
    
    public function paginar($url, $paginaAtual, $totalResultados, $linhasPorPagina, $limiar) {
        
        //numero de páginas necessárias    
        $this->total_paginas = ceil($totalResultados / $linhasPorPagina);
        $this->pagina_atual = $paginaAtual;
        $this->base_url = $url;
        $this->limiar = $limiar;
        
        //$this->query_string = str_replace("&page=".$this->pagina_atual, "", $this->query_string);
    
        if ($this->primeira) {
            $str_primeira = ($this->str_primeira) ? $this->str_primeira : "&laquo;";
            $this->paginacao .= "<a class='btn btn-sm btn-special' href='".$this->base_url."&page=1' title='primeira'>".$str_primeira."</a>&nbsp;".$this->item_spacer;
        }
        
        if ($this->anterior) {                
            $str_anterior = ($this->str_anterior) ? $this->str_anterior : "&lsaquo;";
            if ($this->pagina_atual < $this->total_paginas)
                $this->paginacao .= "<a class='btn btn-sm btn-special' href='".$this->base_url."&page=".($this->pagina_atual+1)."' title='primeira'>".$str_anterior."</a>&nbsp;";
            else
                $this->paginacao .= $str_anterior."&nbsp;".$this->item_spacer;
        }              
        
        if ($this->todas_paginas) {
        
            if ($this->total_paginas > 20) {
                
                //pagina atual ta na primeira metade                
                if ($this->pagina_atual < (floor($this->total_paginas/2))) {
                
                    $p_dif = $this->pagina_atual-$this->limiar;
                    $p_ini = ($p_dif < 1) ? 1 : ($p_dif+1);
                    $p_fim = ($p_dif < 1) ? (($this->pagina_atual+$this->limiar)+($p_dif*-1)) : ($this->pagina_atual+$this->limiar);
                    
                    for ($i=$p_ini;$i<=$p_fim;$i++) {
                        if ($this->pagina_atual==$i)
                            $this->paginacao .= "<a class='btn btn-sm btn-special ativo not-active' href='".$this->base_url."&page=".$i."'>".$i."</a>&nbsp;";
                        else
                            $this->paginacao .= "<a class='btn btn-sm btn-special' href='".$this->base_url."&page=".$i."'>".$i."</a>&nbsp;";
                    }
                    $this->paginacao .= "... ";
                    for ($i=($this->total_paginas-9);$i<=$this->total_paginas;$i++) {
                        $this->paginacao .= "<a class='btn btn-sm btn-special' href='".$this->base_url."&page=".$i."'>".$i."</a>&nbsp;";
                    }                        
            
                }
                else {
                    $p_soma = $this->pagina_atual+$this->limiar;
                    $p_ini = ($p_soma > $this->total_paginas) ? ($this->pagina_atual-$this->limiar)-($p_soma-$this->total_paginas)+1 : $this->pagina_atual-$this->limiar-1;
                    $p_fim = ($p_soma > $this->total_paginas) ? $this->total_paginas : $p_soma;
                    
                    for ($i=1;$i<=10;$i++) {
                        $this->paginacao .= "<a class='btn btn-sm btn-special' href='".$this->base_url."&page=".$i."'>".$i."</a>&nbsp;";
                    }
                    $this->paginacao .= "... ";
                    for ($i=$p_ini;$i<=$p_fim;$i++) {
                        if ($this->pagina_atual==$i)
                            $this->paginacao .= "<a class='btn btn-sm btn-special ativo not-active' href='".$this->base_url."&page=".$i."'>".$i."</a>&nbsp;";
                        else
                            $this->paginacao .= "<a class='btn btn-sm btn-special' href='".$this->base_url."&page=".$i."'>".$i."</a>&nbsp;";
                    }  
                    
                }
            
            }
            else {
                for ($i=1;$i<=$this->total_paginas;$i++) {
                    if ($this->pagina_atual==$i)
                        $this->paginacao .= "<a class='btn btn-sm btn-special ativo not-active' href='".$this->base_url."&page=".$i."'>".$i."</a>&nbsp;";
                    else
                        $this->paginacao .= "<a class='btn btn-sm btn-special' href='".$this->base_url."&page=".$i."'>".$i."</a>&nbsp;";                            
                }
            }
        }
        else {
            
            $this->paginacao .= "&nbsp;| <strong>".$this->pagina_atual."</strong>/".$this->total_paginas." |&nbsp;";
            
        }
        
        if ($this->proxima) {                
            $str_proxima = ($this->str_proxima) ? $this->str_proxima : "&rsaquo;";
            if ($this->pagina_atual < $this->total_paginas)
                $this->paginacao .= "<a class='btn btn-sm btn-special' href='".$this->base_url."&page=".($this->pagina_atual+1)."'title='proxima'>".$str_proxima."</a>&nbsp;".$this->item_spacer;
            else
                $this->paginacao .= $str_proxima."&nbsp;".$this->item_spacer;
        }            
        
        if ($this->ultima) {
            $str_ultima = ($this->str_ultima) ? $this->str_ultima : "&raquo;";
            $this->paginacao .= "<a class='btn btn-sm btn-special' href='".$this->base_url."&page=".$this->total_paginas."' title='última'>".$str_ultima."</a>";
        }
    }
    
    public function getPaginacao() { return $this->paginacao; }
    
    
}


?>