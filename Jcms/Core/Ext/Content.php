<?php

namespace Jcms\Core\Ext;

class Content {
		
	private static $chars = array(
							array(" ","~","^","'","¨","`","´","ç","á","à","ä","â","ã","é","è","ê","ë","í","ì","ï","î","ó","ò","ô","ö","õ","ú","ù","ü","û","ÿ","ý"),
							array("-","_","_","_","_","_","_","c","a","a","a","a","a","e","e","e","e","i","i","i","i","o","o","o","o","o","u","u","u","u","y","y")
							);		
	
	private static $ufs = array("","AC","AL","AM","AP","BA","CE","DF","ES","GO","MA","MG","MS","MT","PA","PB","PE","PI","PR","RJ","RN","RO","RN","RR","RR","RS","SC","SE","SP","TO");
	
	//gera uma URL a partir do titulo
	public static function gerarURL ($titulo) 
	{
			$url = str_ireplace(self::$chars[0],self::$chars[1],strtolower($titulo));
			$url_wsc = "";
			for ($i=0;$i<strlen($url);$i++) {
				if (((ord($url[$i])==32) || (ord($url[$i])==45) || (ord($url[$i])==46) || (ord($url[$i])==95)) || (((ord($url[$i]) >= 48) && (ord($url[$i]) <= 57)) || ((ord($url[$i]) >= 65) && (ord($url[$i]) <= 90)) || ((ord($url[$i]) >= 97) && (ord($url[$i]) <=  122))))
					$url_wsc .= $url[$i];
			}
			return $url_wsc;
	}
	
	public static function removeHTMLTags(&$text) 
	{        
		$new_text = preg_replace("/<([aA-zZ][aA-zZ0-9]*)\b[^>]*>/","",$text);
		$new_text = preg_replace("/<\/([aA-zZ][aA-zZ0-9]*)>/","",$new_text);
		return $new_text;
		
	}
	
	public static function estado2Text($uf_num) 
	{
		if (isset(self::$ufs[$uf_num]) && !empty(self::$ufs[$uf_num]))
			return self::$ufs[$uf_num];
		else
			return " - ";
	}
	
	/**
	 * Gera link em textos a partir de strings do tipo www. ou http://
	*/
	public static function gerarLink($text) 
	{
			$arrayLinks = array();
			//$urlPattern = "/[http[s]?:\/\/|ftp:\/\/]|www\..[a-zA-Z0-9-\.]+\.(com|org|net|mil|edu|ca|co.uk|com.au|gov|br)/";
			$urlPattern = "/[http[s]?:\/\/|ftp:\/\/]|www\..[a-zA-Z0-9-\.]+\.(com|org|net|mil|edu|ca|co.uk|com.au|gov|br)(\/[a-zA-Z0-9-\.-_]*)*/";
			preg_match_all($urlPattern, $text, $arrayLinks);
			
			$linksText = $arrayLinks[0];
			$linksHTML = array();
			
			foreach ($linksText as $link) {
					//$link = strtolower($link);
					$href = (substr($link, 0, 3) === "www") ? "http://".$link : $link;				
					$linkHTML = "<a href=\"".$href."\" target=\"_blank\">".$link."</a>";
					array_push($linksHTML, $linkHTML);
			}
			
			$newText = str_replace($linksText, $linksHTML, $text);
			
			return $newText;
			
	}

	public static function convertToEntities($str) 
	{
		return htmlentities($str,ENT_QUOTES,'ISO-8859-1',true);
	}
	
}