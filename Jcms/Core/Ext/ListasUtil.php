<?php

namespace Jcms\Core\Ext;

class ListasUtil {
    
    private static $comboEquipes;
    
    public static function listaCategorias() {
        
        $array_categorias=array();
        
        $categoria=new Categoria();
        CategoriasDAO::setObject($categoria);
        CategoriasDAO::addSorter(array("nome","ASC"));
        CategoriasDAO::DBConnection();
        CategoriasDAO::listItems(0,0);            
        
        $rowIndex=0;
        $numRows=$categoria->getNumRows();
        
        while ($rowIndex<$numRows) {
            CategoriasDAO::fillObject();
            $array_categorias[$categoria->getCategoriaID()] = $categoria->getNome();
            $rowIndex++;
        }
        
        return $array_categorias;
        
    }
    
    public static function getImovelTipo($imovel_tipo_id) {
        $label_equipe="-";
        if (isset($_SESSION[LISTAS][LIST_IMOVES_TIPOS][$imovel_tipo_id])) {
            $label_equipe=$_SESSION[LISTAS][LIST_IMOVES_TIPOS][$imovel_tipo_id];   
        }
        else {
            $equipe = new ImovelTipo($imovel_tipo_id);
            ImoveisTiposDAO::setObject($equipe);
            ImoveisTiposDAO::DBConnection();
            ImoveisTiposDAO::getObjectDBData();
            
            if ($equipe->getNumRows()>0) {
                $label_equipe=$equipe->getNome();
                $_SESSION[LISTAS][LIST_IMOVES_TIPOS][$imovel_tipo_id]=$label_equipe;
            }
            
        }
        return $label_equipe;            
    }       

}


?>