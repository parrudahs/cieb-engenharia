<?php

namespace Jcms\Core\Ext;

class Image {

    private $name;
    private $ext;
    public $height;
    public $width;
    private $path;
    private $source;
    public $type;


    public function __construct($name, $path) {
        $this->name=$name;
        $this->path=$path;
        $this->ext=strtolower(substr(strrchr($this->name, chr(46)),1));
    }


    public function createImageFromFile() {
        switch ($this->ext){
            case ("jpg") :
                $this->source = imagecreatefromjpeg($this->path."/".$this->name);
                break;
            case ("jpeg") :
                $this->source = imagecreatefromjpeg($this->path."/".$this->name);
                break;
            case ("gif") :
                $this->source = imagecreatefromgif($this->path."/".$this->name);
                break;
            case ("png") :
                $this->source = imagecreatefrompng($this->path."/".$this->name);
                break;
            default : $this->source = imagecreatefromjpeg($this->path."/".$this->name);
        }
    }

    public function createNewImage() {
        $this->source = imagecreatetruecolor($this->width, $this->height);
    }

    public function outputImage($file_name=null) {

        $pathOut = (!empty($file_name)) ? $this->path . "/" . $file_name : null;

        if ($pathOut) {
            switch ($this->ext) {
                case ("jpg")  :
                    imagejpeg($this->source, $pathOut, 100);
                    break;
                case ("jpeg") :
                    imagejpeg($this->source, $pathOut, 100);
                    break;
                case ("gif")  :
                    imagegif($this->source, $pathOut);
                    break;
                case ("png")  :
                    imagepng($this->source, $pathOut,9,PNG_ALL_FILTERS);
                    break;
                default :
                    imagejpeg($this->source, $pathOut, 100);
            }
        }
        else {
            
            
            
            switch ($this->ext) {
                case ("jpg")  :
                    imagejpeg($this->source, null, 100);
                    break;
                case ("jpeg") :
                    imagejpeg($this->source, null, 100);
                    break;
                case ("gif")  :
                    imagegif($this->source);
                    break;
                case ("png")  :
                    imagepng($this->source,null,9,PNG_ALL_FILTERS);
                    break;
                default :
                    imagejpeg($this->source, null, 100);
            }            
        }
        
    }

    public function setDimensions($x, $y) {
        $this->width=$x;
        $this->height=$y;
    }

    public function getDimensionsFromImage() {
        list($this->width, $this->height, $this->type) = getimagesize($this->path."/".$this->name);
    }

    public function getDimensions() {
        $dimensions[0] = $this->width;
        $dimensions[1] = $this->height;
        return $dimensions;
    }

    public function cutImage($ini_x, $ini_y, $w, $h) {
        $cuttedImage = new Image("imgDestaque.jpg",$this->path);
        $cuttedImage->setDimensions($w, $h);
        $cuttedImage->createNewImage();
        $this->createImageFromFile();

        imagecopy($cuttedImage->source, $this->source, 0, 0, $ini_x, $ini_y, $w, $h);

        $cuttedImage->outputImage(0);
    }

    public function resizeImage($maxWidth, $maxHeight, $prop, $new_file_name=null, $new_file_path=null) {

        $this->createImageFromFile();
        $this->getDimensionsFromImage();
        //list($largura_orig, $altura_orig) = getimagesize($nomeOrig);

        if ($prop) {

            $newWidth = $this->width;
            $newHeight = $this->height;


            while (($newWidth > $maxWidth) || ($newHeight > $maxHeight)) {

                if ($newWidth > $maxWidth) {

                    $newWidth = $maxWidth;
                    $perc = ($maxWidth/$this->width); //em quantos porcento a largura foi reduzida ?
                    $newHeight = $this->height*$perc; //altura ser� reduzida proporcionalmente

                }

                //se altura original maior que a m�xima
                else if ($newHeight > $maxHeight) {

                    $newHeight = $maxHeight;
                    $perc = ($maxHeight/$this->height);
                    $newWidth = $this->width*$perc;
                }

                else if (($this->width <= $maxWidth) && ($this->height <= $maxHeight)) {

                    $newWidth = $this->width;
                    $newHeight = $this->height;
                }
            }
        }

        else {
            $newWidth = $maxWidth;
            $newHeight = $maxHeight;
        }

        $path = empty($new_file_path) ? $this->path : $new_file_path;
        
        $resizedImage = new Image($new_file_name, $path);
        $resizedImage->setDimensions($newWidth, $newHeight);
        $resizedImage->createNewImage();

        if (($this->ext=="png") || ($this->ext=="gif")) {
            imagealphablending($resizedImage->source, FALSE);
            imagesavealpha($resizedImage->source, TRUE);            
            $transparent = imagecolorallocatealpha($resizedImage->source, 255, 255, 255, 127);
            imagefilledrectangle($resizedImage->source, 0, 0, $newWidth, $newHeight, $transparent);
        }
    
        imagecopyresampled($resizedImage->source, $this->source, 0, 0, 0, 0, $newWidth, $newHeight, $this->width, $this->height);

        $resizedImage->outputImage($new_file_name);
        
    }
    
    public function setName($name) { $this->name=$name; }
    public function setSource($source) { $this->source=$source; }
    
    public function getSource() { return $this->source(); }    


}

?>