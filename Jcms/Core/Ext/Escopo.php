<?php

namespace Jcms\Core\Ext;

class Escopo {
    
    public static function  setValues($type, $name, &$values) {
        $_SESSION[$type][$name] = serialize($values);
    }
    
    public static function getValues($type, $name) {
        return unserialize($_SESSION[$type][$name]);
    }
    
    public static function isSettled($type, $name) {
        if ($type=="lists") {
            if (isset($_SESSION[$type][$name]) && (sizeof(unserialize($_SESSION[$type][$name])) > 0))
                return true;
            else
                return false;
            
        }
        else if ($type=="instances") {
            if (isset($_SESSION[$type][$name]) && is_object(unserialize($_SESSION[$type][$name])))
                return true;
            else
                return false;               
        }
    }
    
    public static function unLoadValues($type, $name) {
        unset($_SESSION[$type][$name]);
    }
    
}
    
?>