<?php

namespace Jcms\Core\Ext;

class Forms {
    
    private $formName;
    
    public static function setFormName($name) {
        $this->formName = $name;
    }
    
    /**
     * seta o formulario
     */
    public static function setFormData(&$data, $nome) {
        $_SESSION['formulario_'.$nome]= $data;
    }


    /**
     * Apagar a sessão do formulario
     */
    public static function unsetFormData($nome) {
        unset($_SESSION['formulario_'.$nome]);
    }
    
}

?>