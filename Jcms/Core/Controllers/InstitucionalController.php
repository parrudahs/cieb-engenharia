<?php


namespace Jcms\Core\Controllers;

use Jcms\Core\Models\Dao\ConteudoDAO;
use Jcms\Core\Models\Conteudo;
use Intervention\Image\ImageManagerStatic as Image;
use Jcms\Core\Ext\Content;

class InstitucionalController extends ConteudoDAO
{

    private $institucional;
    private $diretorioUpload; 

    public function __construct()
    {
        parent::__construct();
        $this->institucional = new Conteudo();
        $this->diretorioUpload = 'public/uploaded_files/institucional/';
    }


    /**
     * @return mixed
     */
    public function listaInstitucional()
    {
        $this->addFilter(array('tipo', '=', 1));
        return $this->listItems(0,0,true);
    }

    public function showPageInstitucional()
    {
        $this->addFilter(array('url', '=', 'institucional'));
        $data = $this->listItems(0,1,true);
        return $data[0];
    }

    /**
     * 
     */
    public function show($id)
    {
        $this->addFilter(array('id', '=', $id));        
        $data = $this->listItems(0,1);
        $this->Formulario($data[0]);
        return $data[0];
    }

    /**
     * 
     */
    public function Formulario($data) 
    {
        unset($_SESSION['formulario_inst']);
        $this->institucional->setTitulo(isset($data['titulo']) ? $data['titulo'] : null);
        $this->institucional->setResumo(isset($data['resumo']) ? $data['resumo'] : null);
        $this->institucional->setTipo(1); // 1 = institucional
        $this->institucional->setCorpo(isset($data['corpo']) ? $data['corpo'] : null);
        $this->institucional->setBanner(isset($data['banner']) ? $data['banner'] : null);
        $this->institucional->setUrl(Content::gerarURL($data['titulo']));

        $_SESSION['formulario_inst']['titulo'] = $this->institucional->getTitulo();
        $_SESSION['formulario_inst']['resumo'] = $this->institucional->getResumo();
        $_SESSION['formulario_inst']['banner'] = $this->institucional->getBanner();
        $_SESSION['formulario_inst']['corpo'] = $this->institucional->getCorpo();
    }


    /**
     * 
     */
    public function updateImagem($data, $id) 
    {
        if($_FILES['banner']['type'] != 'image/jpeg' && $_FILES['banner']['type'] != 'image/png' && $_FILES['banner']['type'] != 'image/gif') {
            $_SESSION['output_message'] = 'O formato da Imagem não é permitido';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;
        }

        $temp = explode(".", $_FILES['banner']['name']);
        $data['banner'] = time().uniqid(rand()).'.'.end($temp);

        //remove a imagem do servidor
        $image = $this->show($id);
        unlink($this->diretorioUpload.$image['banner']);

        $this->Formulario($data);

        if($this->updateItem($this->institucional, $id)) {

            $imagem = Image::make($_FILES['banner']['tmp_name']);

            if($imagem->width() != 1100 || $imagem->height() != 440)
                $imagem->resize(1100,440);

            if($imagem->save($this->diretorioUpload.$data['banner'])) {
                $_SESSION['output_message'] = 'Conteúdo atualizado com sucesso!';
                $_SESSION['output_message_tipo'] = 'success';
                unset($_SESSION['formulario_inst']);
                return 1;

            }

            $_SESSION['output_message'] = 'erro no upload da imagem!';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;
        }

        $_SESSION['output_message'] = 'Não foi possível atualizar o conteudo!';
        $_SESSION['output_message_tipo'] = 'danger';
        return 0;

    }

    /**
     * Método para atualizar
     * @param $data
     * @param $id
     */
    public function update($data, $id)
    {
        $this->Formulario($data);

        if (empty($this->institucional->getTitulo()) || empty($this->institucional->getCorpo())) {
            $_SESSION['output_message'] = 'Conteudo não atualizado!<br/>Campos marcados com * são obrigatórios.';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;
        }
        if(empty($_FILES['banner']['name'])) {
            
            $imagem = $this->show($id);
            $data['banner'] = $imagem['banner'];
            $this->Formulario($data);

            if($this->updateItem($this->institucional, $id)) {
                $_SESSION['output_message'] = 'Conteúdo atualizado com sucesso!';
                $_SESSION['output_message_tipo'] = 'success';
                unset($_SESSION['formulario_inst']);
                return 1;
            }

            $_SESSION['output_message'] = 'Não foi possível atualizar o conteudo!';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;
        }
        return $this->updateImagem($data, $id);
    }

    
}