<?php
namespace Jcms\Core\Controllers;


use Jcms\Core\Models\Dao\TagDAO;
use Jcms\Core\Models\Tag;

class TagController extends TagDAO
{


    private $tag;

    /**
     * 
     */
    public function __construct()
    {
        parent::__construct();
        $this->tag = new Tag();
    }


    /**
     * @return array|mixed
     */
    public function listaTodasTags()
    {
        $this->clearFilter();
        $this->addSorter(array("nome","ASC"));
        return $this->listItems(0,0,true);
    }

    /**
     * 
     */
    public function show($id)
    {
        $this->addFilter(array('id', '=', $id));
        $data = $this->listItems(0,0);
        $this->Formulario($data[0]);
        return $data[0];
    }



    /**
     * 
     */
    public function Formulario($data) 
    {
        unset($_SESSION['formulario_tag']);

        $this->tag->setNome(isset($data['nome']) ? $data['nome'] : null);

        $_SESSION['formulario_tag']['nome'] = $this->tag->getNome();
    }

    /**
     * 
     */
    public function create($data) 
    {
        $this->Formulario($data);

        if (empty($this->tag->getNome())) {
            $_SESSION['output_message'] = 'Tag não cadastrada!<br/>Campos marcados com * são obrigatórios.';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;
        }

        if($this->insertItem($this->tag)) {
            $_SESSION['output_message'] = 'Tag cadastrada com sucesso!';
            $_SESSION['output_message_tipo'] = 'success';
            unset($_SESSION['formulario_tag']);
            return 1;
        }

        $_SESSION['output_message'] = 'Não foi possível cadastrar a Tag!';
        $_SESSION['output_message_tipo'] = 'danger';
        return 0;
    }

    /**
     * 
     */
    public function update($data, $id)
    {
        $this->Formulario($data);
        if (empty($this->tag->getNome())) {
            $_SESSION['output_message'] = 'Tag não cadastrada!<br/>Campos marcados com * são obrigatórios.';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;
        }

        if($this->updateItem($this->tag, $id)) {
            $_SESSION['output_message'] = 'Tag atualizada com sucesso!';
            $_SESSION['output_message_tipo'] = 'success';
            return 1;
        }

        $_SESSION['output_message'] = 'Não foi possível atualizar a Tag!';
        $_SESSION['output_message_tipo'] = 'danger';
        return 0;

    }

    /**
     * 
     */
    public function delete($id)
    {

        if($this->deleteItem($id)) {
            $_SESSION['output_message'] = 'Tag deletada com sucesso!';
            $_SESSION['output_message_tipo'] = 'success';
            return 1;
        }

        $_SESSION['output_message'] = 'Erro ao deletar Tag!';
        $_SESSION['output_message_tipo'] = 'danger';
        return 0;
    }


}