<?php
namespace Jcms\Core\Controllers;


use Jcms\Core\Models\Dao\PortfolioDAO;
use Jcms\Core\Models\Dao\FotoDAO;
use Jcms\Core\Models\Dao\TagDAO;
use Jcms\Core\Models\Portfolio;

class PortfolioController extends PortfolioDAO
{


    private $portfolio;
    private $tags;
    private $foto;
    private $diretorioUpload;

    /**
     * 
     */
    public function __construct()
    {
        parent::__construct();
        $this->portfolio = new Portfolio();
        $this->foto = new FotoDAO();
        $this->tags = new TagDAO();
        $this->diretorioUpload = "public/uploaded_files/portfolio/";
    }


    public function getTags()
    {
        return $this->tags->listItems(0,0);
    }

    /**
     * @return array|mixed
     */
    public function listaTodosTrabalhos($publicado = false, $capa = false, $porTag = null)
    {
        $this->clearFilter();
        $this->addSorter(array("data","DESC"));
        $this->addSorter(array("nome","ASC"));

        if(!is_null($porTag))
            $this->addFilter(array('tag_id', '=', $porTag));    

        if($publicado)
            $this->addFilter(array('publicado', '=', '1'));    

        $retorno = $this->listItems(0,0,true);

        if($capa) {
            $i = 0;
            foreach ($retorno as $r) {
                $this->clearFilter();
                $this->foto->addFilter(array('capa', '=' , '1'));
                $this->foto->addFilter(array('publicar', '=' , '1'));
                $this->foto->addFilter(array('portfolio_id', '=', $r['id']));
                $this->foto->addSorter(array("RAND()",""));

                $capa = $this->foto->listItems(0,0,true);

                if($this->foto->getRowCount() > 0)
                    $retorno[$i]['imagem'] = $capa[0]['imagem'];

                $i++;
            }
        }
        return $retorno;
    }

    public function showPortfolio($id)
    {

        $this->addFilter(array('id', '=', $id));
        $data = $this->listItems(0,1,true);

        //verifica se retornou algum resultado        
        if(!isset($data[0]))
            header('Location:'.BASE_URL.'404');

        return $data[0];
    }

    /**
     * 
     */
    public function show($id)
    {
        $this->addFilter(array('id', '=', $id));
        $data = $this->listItems(0,0);
        $this->Formulario($data[0]);
        return $data[0];
    }



    /**
     * 
     */
    public function Formulario($data) 
    {
        unset($_SESSION['formulario_portfolio']);

        $this->portfolio->setNome(isset($data['nome']) ? $data['nome'] : null);
        $this->portfolio->setLocal($data['local']);
        $this->portfolio->setEstado(isset($data['estado']) ? $data['estado'] : null);
        $this->portfolio->setSigla(isset($data['sigla']) ? $data['sigla'] : null);
        $this->portfolio->setDescricao(isset($data['descricao']) ? $data['descricao'] : null);
        $this->portfolio->setTagId(isset($data['tag_id']) ? $data['tag_id'] : null, 0);
        $this->portfolio->setPublicado(isset($data['publicado']) ? $data['publicado'] : '0');

        $_SESSION['formulario_portfolio']['nome'] = $this->portfolio->getNome();
        $_SESSION['formulario_portfolio']['local'] = $this->portfolio->getLocal();
        $_SESSION['formulario_portfolio']['estado'] = $this->portfolio->getEstado();
        $_SESSION['formulario_portfolio']['sigla'] = $this->portfolio->getSigla();
        $_SESSION['formulario_portfolio']['descricao'] = $this->portfolio->getDescricao();
        $_SESSION['formulario_portfolio']['tag_id'] = $this->portfolio->getTagId();
        $_SESSION['formulario_portfolio']['publicado'] = $this->portfolio->getPublicado();
    }

    /**
     * 
     */
    public function create($data) 
    {
        if(isset($data['publicado']))
            $data['publicado'] = ($data['publicado'] == 'on') ? '1': '0';

        $this->Formulario($data);

        if (empty($this->portfolio->getNome()) || empty($this->portfolio->getTagId() || empty($this->portfolio->getDescricao()))) {
            $_SESSION['output_message'] = 'Portfólio não cadastrado!<br/>Campos marcados com * são obrigatórios.';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;
        }

        if($this->insertItem($this->portfolio)) {
            $_SESSION['output_message'] = 'Portfólio cadastrado com sucesso!';
            $_SESSION['output_message_tipo'] = 'success';
            unset($_SESSION['formulario_gal']);
            return 1;
        }

        $_SESSION['output_message'] = 'Não foi possível cadastrar o Portfólio!';
        $_SESSION['output_message_tipo'] = 'danger';
        return 0;

    }

    /**
     * 
     */
    public function update($data, $id)
    {
        if(isset($data['publicado']))
            $data['publicado'] = ($data['publicado'] == 'on') ? '1': '0';

        $this->Formulario($data);

        if (empty($this->portfolio->getNome()) || empty($this->portfolio->getTagId() || empty($this->portfolio->getDescricao()))) {
            $_SESSION['output_message'] = 'Portfólio não cadastrado!<br/>Campos marcados com * são obrigatórios.';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;
        }

        if($this->updateItem($this->portfolio, $id)) {
            $_SESSION['output_message'] = 'Portfólio atualizado com sucesso!';
            $_SESSION['output_message_tipo'] = 'success';
            return 1;
        }

        $_SESSION['output_message'] = 'Não foi possível atualizar o Portfólio!';
        $_SESSION['output_message_tipo'] = 'danger';
        return 0;
    }

    /**
     * 
     */
    public function delete($id)
    {
        $this->addFilter(array('portfolio_id', '=', $id));
        $imagens = $this->foto->listItems(0,0, true);

        if($this->foto->getRowCount() > 0) {

            foreach ($imagens as $i) {
                unlink($this->diretorioUpload."350x-".$i['imagem']);
                unlink($this->diretorioUpload."1110x-".$i['imagem']);
            }

            $this->foto->deleteAllFotosPortfolio($id);

            if($this->deleteItem($id)) {
                $_SESSION['output_message'] = 'Portfolio deletada com sucesso!';
                $_SESSION['output_message_tipo'] = 'success';
                return 1;
            }
        } else {
            if($this->deleteItem($id)) {                
                $_SESSION['output_message'] = 'Portfolio deletada com sucesso!';
                $_SESSION['output_message_tipo'] = 'success';
                return 1;
            }
        }
        
        $_SESSION['output_message'] = 'Erro ao deletar portfolio!';
        $_SESSION['output_message_tipo'] = 'danger';
        return 0;
    }


}