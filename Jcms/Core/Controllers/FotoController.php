<?php
namespace Jcms\Core\Controllers;

use Jcms\Core\Models\Dao\FotoDAO;
use Jcms\Core\Models\Foto;
use Intervention\Image\ImageManagerStatic as Image;

class FotoController extends FotoDAO
{

    private $foto;
    private $diretorioUpload;

    public function __construct() 
    {
        parent::__construct();
        $this->foto = new Foto();
        $this->diretorioUpload = "public/uploaded_files/portfolio/";
    }

    /**
     * @param $id
     * @return array|mixed
     */
    public function index($id)
    {
        $this->clearFilter();
        $this->addFilter(array("portfolio_id","=",$id));
        $this->addSorter(array("RAND()",""));
        return $this->listItems(0,4, true);
    }

    public function allFotosPortfolio($id)
    {
        $this->clearFilter();
        $this->addFilter(array("portfolio_id","=",$id));
        $this->addFilter(array("publicar","=",'1'));
        return $this->listItems(0,0, true);
    }

    /**
     * 
     */
    public function capaGaleria($id)
    {
        $this->clearFilter();
        $this->addFilter(array("portfolio_id","=",$id));
        $this->addSorter(array("RAND()",""));
        $retorno = $this->listItems(0,1, true);
        return $retorno[0];
    }

    /**
     * Lista Todas as fotos de uma Galeria
     */
    public function getFotosGaleria($id) 
    {
        $this->addFilter(array('portfolio_id','=',$id));
        return $this->listItems(0,0, true);
    } 


    /**
     * 
     */
    public function Formulario($data) 
    {
        $this->foto->setPortfolioId(isset($data['portfolio_id']) ? $data['portfolio_id']: 0);
        $this->foto->setImagem(isset($data['imagem']) ? $data['imagem']: null);
        $this->foto->setDescricao(isset($data['descricao']) ? $data['descricao']: null);
        $this->foto->setCapa(isset($data['capa']) ? $data['capa'] : '0');
        $this->foto->setPublicar(isset($data['publicada']) ? $data['publicada'] : '0');
    }

    /**
     * Metodo que adiciona as fotos
     */
    public function addFotos($id)
    {
        if(!empty($_FILES['file'])) {

            $temp = explode(".",  $_FILES['file']['name']);

            $data['imagem'] = time().uniqid(rand()).'.'.end($temp);

            $data['portfolio_id'] = $id;
            $data['publicada'] = '1';

            $this->Formulario($data);

            if( $_FILES['file']['type'] == 'image/jpeg' || $_FILES['file']['type'] == 'image/png'  || 
                $_FILES['file']['type'] == 'image/gif'){
                
                if($this->insertItem($this->foto)) {

                    $upload_arq = $this->diretorioUpload."350x-".$data['imagem'];

                    if(Image::make($_FILES['file']['tmp_name'])->widen(350)->save($upload_arq)){
                        $upload_arq = $this->diretorioUpload."1110x-".$data['imagem'];
                        Image::make($_FILES['file']['tmp_name'])->widen(1110)->save($upload_arq);
                        
                        $_SESSION['output_message'] = 'As fotos foram adicionadas com sucesso!';
                        $_SESSION['output_message_tipo'] = 'success';
                        return 1;
                    }
                }

            } else {
                $_SESSION['output_message'] = 'O formato da Imagem não é permitido';
                $_SESSION['output_message_tipo'] = 'danger';
                return 0;
            }
        } else {
            $_SESSION['output_message'] = 'Erro';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;
        }
        
    }

    /**
     * Metodo que mostra uma Foto
     */
    public function show($id)
    {
        $this->addFilter(array('id', '=', $id));
        $data = $this->listItems(0,1);
        $this->Formulario($data[0]);
        return $data[0];
    }

    /**
     * Atualiza Foto
     */
    public function update($data, $id)
    {
        $data['capa'] = (isset($data['capa']) && $data['capa'] == 'on') ? '1' : '0';
        $data['publicada'] = ($data['publicada'] == 'on') ? '1': '0';

        var_dump($data);        
        $this->Formulario($data);   

        if($this->updateItem($this->foto, $id)) {
            //$_SESSION['output_message'] = 'Foto atualizada com sucesso!';
            //$_SESSION['output_message_tipo'] = 'success';
            return 1;
        }

        //$_SESSION['output_message'] = 'Não foi possível atualizar a Foto!';
        //$_SESSION['output_message_tipo'] = 'danger';
        return 0;       
    }

    /**
     * Deletar Foto
     */
    public function delete($id)
    {
        $imagem = $this->show($id);

        if($this->deleteItem($id)) {
            //remove a imagem do servidor
            unlink($this->diretorioUpload."350x-".$imagem['imagem']);
            unlink($this->diretorioUpload."1110x-".$imagem['imagem']);

            $_SESSION['output_message'] = 'A foto foi deletada com sucesso!';
            $_SESSION['output_message_tipo'] = 'success';
            return 1;
        } else {
            $_SESSION['output_message'] = 'Erro ao deletar a noticia!';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;
        }
    }


}