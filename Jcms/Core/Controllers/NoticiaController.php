<?php
namespace Jcms\Core\Controllers;

use Jcms\Core\Models\Dao\ConteudoDAO;
use Jcms\Core\Models\Conteudo;
use Jcms\Core\Ext\Content;
use Intervention\Image\ImageManagerStatic as Image;
use Jcms\Core\Ext\Forms;

class NoticiaController extends ConteudoDAO
{
    private $conteudo;
    private $diretorioUpload; 

    public function __construct()
    {
        parent::__construct();
        $this->conteudo = new Conteudo();
        $this->diretorioUpload = 'public/uploaded_files/noticias/';
    }

    /**
     * Lista todos as noticias cadastrados no BD
     */
    public function listaTodasNoticiasPainel()
    {
        $this->addFilter(array("tipo","=",3));
        return $this->listItems(0,0, true);
    }

    /**
     * noticias na pagina inicial
     */
    public function listaNoticiasHome($limite=null)
    {
        $this->addFilter(array("tipo","=",3));
        $this->addFilter(array("publicado","=",1));
        return $this->listItems(0, $limite != null ? (int) $limite : 0, true);
    }


        /**
     * noticias na pagina inicial
     */
    public function listaNoticias($pagina, $rowPerPage)
    {
        $pagina = $pagina != null ? (int) $pagina : 1;
        $this->addFilter(array("tipo","=",3));
        $this->addFilter(array("publicado","=",1));        
        return $this->listItems((($pagina-1)*$rowPerPage),$rowPerPage, true);
    }


    /**
     * 
     */
    public function showPageNoticia($url)
    {
        $this->addFilter(array('url', '=', $url));
        $data = $this->listItems(0,1,true);

        //verifica se retornou algum resultado        
        if(!isset($data[0]))
            header('Location:'.BASE_URL.'404');

        return $data[0];
    }

    /**
     * @param $id
     */
    public function show($id)
    {
        $this->addFilter(array('id', '=', $id));
        $data = $this->listItems(0,1,true);
        $this->Formulario($data[0]);
        return $data[0];
    }

    /**
     * 
     */
    public function Formulario($data) 
    {
        unset($_SESSION['formulario_noticias']);

        $this->conteudo->setUrl(Content::gerarURL($data['titulo']));
        $this->conteudo->setTitulo($data['titulo']);
        $this->conteudo->setTipo(3); // tipo 3 = noticias
        $this->conteudo->setResumo(isset($data['resumo']) ? $data['resumo'] : null);
        $this->conteudo->setCorpo(isset($data['corpo']) ? $data['corpo'] : null);
        $this->conteudo->setBanner(isset($data['banner']) ? $data['banner'] : null);
        $this->conteudo->setPublicado(isset($data['publicado']) ? $data['publicado'] : null);

        Forms::setFormData($data, 'noticias');
    }

    /**
     * cria um novo serviço
     */
    public function create($data) 
    {
        $temp = explode(".", $_FILES['imagem']['name']);
        $data['banner'] = time().uniqid(rand()).'.'.end($temp);
        $data['publicado'] = ($data['publicado'] == 'on') ? '1': '0';
        $this->Formulario($data);

        if (empty($this->conteudo->getTitulo()) || empty($this->conteudo->getCorpo())) {
            $_SESSION['output_message'] = 'Noticia não cadastrada!<br/>Campos marcados com * são obrigatórios.';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;
        }

        if($_FILES['imagem']['type'] != 'image/jpeg' && $_FILES['imagem']['type'] != 'image/png' &&  $_FILES['imagem']['type'] != 'image/gif') {
            $_SESSION['output_message'] = 'O formato da Imagem não é permitido';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;
        }

        if($this->insertItem($this->conteudo)) {
            $upload_arq = $this->diretorioUpload.$data['banner'];

            if(Image::make($_FILES['imagem']['tmp_name'])->widen(1100)->save($upload_arq)) {
                $_SESSION['output_message'] = 'Noticia cadastrada com sucesso!';
                $_SESSION['output_message_tipo'] = 'success';
                unset($_SESSION['formulario_noticias']);
                return 1;
            }

            $_SESSION['output_message'] = 'erro no upload da imagem!';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;
        }

        $_SESSION['output_message'] = 'Não foi possível cadastrar a noticia!';
        $_SESSION['output_message_tipo'] = 'danger';
        return 0;

    }

    /**
     * 
     */
    public function updateImagem($data, $id) 
    {
        if($_FILES['imagem']['type'] != 'image/jpeg' && $_FILES['imagem']['type'] != 'image/png' &&  $_FILES['imagem']['type'] != 'image/gif') {
            $_SESSION['output_message'] = 'O formato da Imagem não é permitido';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;
        }

        $temp = explode(".", $_FILES['imagem']['name']);
        $data['banner'] = time().uniqid(rand()).'.'.end($temp);
        $image = $this->show($id);
        unlink($this->diretorioUpload.$image['banner']);
        $data['publicado'] = ($data['publicado'] == 'on') ? '1': '0';
        $this->Formulario($data);

        if($this->updateItem($this->conteudo, $id)) {
            $upload_arq = $this->diretorioUpload.$data['banner'];

            if(Image::make($_FILES['imagem']['tmp_name'])->widen(1100)->save($upload_arq)) {
                $_SESSION['output_message'] = 'Noticia atualizada com sucesso!';
                $_SESSION['output_message_tipo'] = 'success';
                return 1;
            }

            $_SESSION['output_message'] = 'erro no upload da imagem!';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;
        }

        $_SESSION['output_message'] = 'Não foi possível atualizar a noticia!';
        $_SESSION['output_message_tipo'] = 'danger';
        return 0;
    }

    /**
     * Método para atualizar um destaque
     * @param $data
     * @param $id
     */
    public function update($data, $id)
    {
        $this->Formulario($data);
        if (empty($this->conteudo->getTitulo()) || empty($this->conteudo->getCorpo())) {
            $_SESSION['output_message'] = 'Noticia não atualizado!<br/>Campos marcados com * são obrigatórios.';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;
        }

        if(empty($_FILES['imagem']['name'])) {

            $image = $this->show($id);
            $data['banner'] = $image['banner'];
            $data['publicado'] = ($data['publicado'] == 'on') ? '1': '0';
            $this->Formulario($data);

            if($this->updateItem($this->conteudo, $id)) {
                $_SESSION['output_message'] = 'Noticia atualizada com sucesso!';
                $_SESSION['output_message_tipo'] = 'success';
                return 1;
            }

            $_SESSION['output_message'] = 'Não foi possível atualizar a noticia!';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;
        }
        return $this->updateImagem($data, $id);
    }

    /**
     * Deleta um serviço
     */
    public function delete($id)
    {
        $imagem = $this->show($id);

        if($this->deleteItem($id)) {
            //remove a imagem do servidor
            unlink($this->diretorioUpload.$imagem['banner']);

            $_SESSION['output_message'] = 'Noticia deletada com sucesso!';
            $_SESSION['output_message_tipo'] = 'success';
            return 1;
        }

        $_SESSION['output_message'] = 'Erro ao deletar o serviço!';
        $_SESSION['output_message_tipo'] = 'danger';
        return 0;
    }

}