<?php
namespace Jcms\Core\Controllers;

use Jcms\Core\Models\Dao\ConteudoDAO;
use Jcms\Core\Models\Conteudo;
use Jcms\Core\Ext\Content;
use Jcms\Core\Ext\DataHr;
use Intervention\Image\ImageManagerStatic as Image;

class ServicoController extends ConteudoDAO
{
    private $conteudo;
    private $diretorioUpload; 

    public function __construct()
    {
        parent::__construct();
        $this->conteudo = new Conteudo();
        $this->diretorioUpload = 'public/uploaded_files/servicos/';
    }

    /**
     * Lista todos os serviços cadastrados no BD
     */
    public function listaTodosServicosPainel()
    {
        $this->addFilter(array("tipo","=",2));
        return $this->listItems(0,0, true);
    }

    public function listaTodosServicos() 
    {
        $this->clearFilter();
        $this->addFilter(array("tipo","=",2));
        $this->addFilter(array("publicado","=",1));
        return $this->listItems(0,0, true);
    }


    public function showServico($url)
    {
        $this->clearFilter();
        $this->addFilter(array('url', '=', $url));
        $this->addFilter(array('publicado', '=', 1));
        $data = $this->listItems(0,1,true);

        //verifica se retornou algum resultado   
        if(!isset($data[0]))
            header('Location:'.BASE_URL.'404');

        return $data[0];
    }


    /**
     * @param $id
     */
    public function show($id)
    {
        $this->addFilter(array('id', '=', $id));
        $data = $this->listItems(0,1,true);
        $this->Formulario($data[0]);
        return $data[0];
    }

    /**
     * 
     */
    public function Formulario($data) 
    {
        unset($_SESSION['formulario_servico']);

        $this->conteudo->setUrl(Content::gerarURL($data['titulo']));
        $this->conteudo->setTitulo($data['titulo']);
        $this->conteudo->setTipo(2); // tipo 2 = servico
        $this->conteudo->setResumo(isset($data['resumo']) ? $data['resumo'] : null);
        $this->conteudo->setCorpo(isset($data['corpo']) ? $data['corpo'] : null);
        $this->conteudo->setBanner(isset($data['imagem_destaque']) ? $data['imagem_destaque'] : null);
        $this->conteudo->setPublicado(isset($data['publicado']) ? $data['publicado'] : null);

        $_SESSION['formulario_servico']['titulo'] = $this->conteudo->getTitulo();
        $_SESSION['formulario_servico']['banner'] = $this->conteudo->getBanner();
        $_SESSION['formulario_servico']['corpo'] = $this->conteudo->getCorpo();
        $_SESSION['formulario_servico']['resumo'] = $this->conteudo->getResumo();
        $_SESSION['formulario_servico']['url'] = $this->conteudo->getUrl();
        //$_SESSION['formulario_servico']['data'] = $this->conteudo->getData();
        $_SESSION['formulario_servico']['publicado'] = $this->conteudo->getPublicado();
    }

    /**
     * cria um novo serviço
     */
    public function create($data) 
    {
        $temp = explode(".", $_FILES['imagem']['name']);
        $data['imagem_destaque'] = time().uniqid(rand()).'.'.end($temp);
        $data['publicado'] = ($data['publicado'] == 'on') ? '1': '0';
        $this->Formulario($data);

        if (empty($this->conteudo->getTitulo()) || empty($this->conteudo->getCorpo())) {
            $_SESSION['output_message'] = 'Serviço não cadastrado!<br/>Campos marcados com * são obrigatórios.';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;
        }

        if($_FILES['imagem']['type'] != 'image/jpeg' &&  $_FILES['imagem']['type'] != 'image/png' && $_FILES['imagem']['type'] != 'image/gif') {
            $_SESSION['output_message'] = 'O formato da imagem não é permitido';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;
        }

        if($this->insertItem($this->conteudo)) {

            $upload_arq = $this->diretorioUpload.$data['imagem_destaque'];

            if(Image::make($_FILES['imagem']['tmp_name'])->widen(1100)->save($upload_arq)) {
                $_SESSION['output_message'] = 'Serviço cadastrado com sucesso!';
                $_SESSION['output_message_tipo'] = 'success';
                unset($_SESSION['formulario_servico']);
                return 1;

            }

            $_SESSION['output_message'] = 'erro no upload da imagem!';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;
        }

        $_SESSION['output_message'] = 'Não foi possível cadastrar o serviço!';
        $_SESSION['output_message_tipo'] = 'danger';
        return 0;
    }

    /**
     * 
     */
    public function updateImagem($data, $id) 
    {
        if($_FILES['imagem']['type'] != 'image/jpeg' &&  $_FILES['imagem']['type'] != 'image/png' &&  $_FILES['imagem']['type'] != 'image/gif') {
            $_SESSION['output_message'] = 'O formato da Imagem não é permitido';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;
        }

        $temp = explode(".", $_FILES['imagem']['name']);
        $data['imagem_destaque'] = time().uniqid(rand()).'.'.end($temp);
        //remove a imagem do servidor
        $image = $this->show($id);
        unlink($this->diretorioUpload.$image['banner']);
        $data['publicado'] = ($data['publicado'] == 'on') ? '1': '0';
        $this->Formulario($data);

        if($this->updateItem($this->conteudo, $id)) {

            $upload_arq = $this->diretorioUpload.$data['imagem_destaque'];

            if(Image::make($_FILES['imagem']['tmp_name'])->widen(1100)->save($upload_arq)) {
                $_SESSION['output_message'] = 'Serviço atualizado com sucesso!';
                $_SESSION['output_message_tipo'] = 'success';
                return 1;
            }

            $_SESSION['output_message'] = 'erro no upload da imagem!';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;
        }

        $_SESSION['output_message'] = 'Não foi possível atualizar o serviço!';
        $_SESSION['output_message_tipo'] = 'danger';
        return 0;
    }

    /**
     * Método para atualizar um destaque
     * @param $data
     * @param $id
     */
    public function update($data, $id)
    {
        $this->Formulario($data);

        if (empty($this->conteudo->getTitulo()) || empty($this->conteudo->getCorpo())) {
            $_SESSION['output_message'] = 'Serviço não atualizado!<br/>Campos marcados com * são obrigatórios.';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;
        }

        if(empty($_FILES['imagem']['name'])) {

            $image = $this->show($id);
            $data['imagem_destaque'] = $image['banner'];
            $data['publicado'] = ($data['publicado'] == 'on') ? '1': '0';
            $this->Formulario($data);

            if($this->updateItem($this->conteudo, $id)) {
                $_SESSION['output_message'] = 'Serviço atualizado com sucesso!';
                $_SESSION['output_message_tipo'] = 'success';
                return 1;
            }

            $_SESSION['output_message'] = 'Não foi possível atualizar o serviço!';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;
        }

        return $this->updateImagem($data, $id);
    }

    /**
     * Deleta um serviço
     */
    public function delete($id)
    {
        $imagem = $this->show($id);

        if($this->deleteItem($id)) {
            //remove a imagem do servidor
            unlink($this->diretorioUpload.$imagem['banner']);

            $_SESSION['output_message'] = 'Serviço deletado com sucesso!';
            $_SESSION['output_message_tipo'] = 'success';
            return 1;
        }

        $_SESSION['output_message'] = 'Erro ao deletar o serviço!';
        $_SESSION['output_message_tipo'] = 'danger';
        return 0;
    }

}