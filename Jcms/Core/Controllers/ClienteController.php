<?php
namespace Jcms\Core\Controllers;

use Jcms\Core\Models\Dao\ClienteDAO;
use Jcms\Core\Models\Cliente;
use Intervention\Image\ImageManagerStatic as Image;
use Jcms\Core\Ext\Forms;

class ClienteController extends ClienteDAO
{
    private $cliente;
    private $diretorioUpload; 

    public function __construct()
    {
        parent::__construct();
        $this->cliente = new Cliente();
        $this->diretorioUpload = 'public/uploaded_files/clientes/';
    }

    /**
     * Lista todos as noticias cadastrados no BD
     */
    public function listaTodosClientesPainel()
    {
        Forms::unsetFormData('cliente');
        return $this->listItems(0,0, true);
    }

    /**
     * 
     */
    public function listaClientesHome($limite)
    {
        return $this->listItems(0,$limite, true);
    }

    /**
     * 
     */
    public function listaTodosClientes()
    {
        return $this->listItems(0,0, true);
    }

    /**
     * @param $id
     */
    public function show($id)
    {
        $this->addFilter(array('id', '=', $id));
        $data = $this->listItems(0,1,true);
        $this->Formulario($data[0]);
        return $data[0];
    }

    /**
     * 
     */
    public function Formulario($data) 
    {
        $this->cliente->setNome($data['nome']);
        $this->cliente->setLogo($data['logo']);
        $this->cliente->setDescricao($data['descricao']);
        $this->cliente->setSite(isset($data['site']) ? $data['site'] : null);
        Forms::setFormData($data, 'cliente');
    }

    /**
     * cria um novo serviço
     */
    public function create($data) 
    {
        $temp = explode(".", $_FILES['imagem']['name']);
        $data['logo'] = time().uniqid(rand()).'.'.end($temp);
        $this->Formulario($data);

        if (empty($this->cliente->getNome()) || empty($_FILES['imagem']['name']) ) {
            $_SESSION['output_message'] = 'Cliente não cadastrado!<br/>Campos marcados com * são obrigatórios.';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;
        }

        if($_FILES['imagem']['type'] != 'image/jpeg' && $_FILES['imagem']['type'] != 'image/png' && $_FILES['imagem']['type'] != 'image/gif') {
            $_SESSION['output_message'] = 'O formato da Imagem não é permitido';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;
        }

        if($this->insertItem($this->cliente)) {

            $upload_arq = $this->diretorioUpload.$data['logo'];

            if(Image::make($_FILES['imagem']['tmp_name'])->widen(100)->save($upload_arq)) {
                $_SESSION['output_message'] = 'Cliente cadastrado com sucesso!';
                $_SESSION['output_message_tipo'] = 'success';
                Forms::unsetFormData('cliente');
                return 1;

            }

            $_SESSION['output_message'] = 'erro no upload da logo!';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;
        }

        $_SESSION['output_message'] = 'Não foi possível cadastrar o cliente!';
        $_SESSION['output_message_tipo'] = 'danger';
        return 0;
    }

    /**
     * 
     */
    public function updateImagem($data, $id) 
    {
        if($_FILES['imagem']['type'] != 'image/jpeg' && $_FILES['imagem']['type'] != 'image/png' && $_FILES['imagem']['type'] != 'image/gif') {
            $_SESSION['output_message'] = 'O formato da Imagem não é permitido';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;
        }

        $temp = explode(".", $_FILES['imagem']['name']);
        $data['logo'] = time().uniqid(rand()).'.'.end($temp);
        //remove a imagem do servidor
        $image = $this->show($id);
        unlink($this->diretorioUpload.$image['logo']);

        $this->Formulario($data);

        if($this->updateItem($this->cliente, $id)) {

            $upload_arq = $this->diretorioUpload.$data['logo'];

            if(Image::make($_FILES['imagem']['tmp_name'])->widen(100)->save($upload_arq)) {
                $_SESSION['output_message'] = 'Cliente atualizado com sucesso!';
                $_SESSION['output_message_tipo'] = 'success';
                Forms::unsetFormData('cliente');
                return 1;
            }

            $_SESSION['output_message'] = 'erro no upload da logo!';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;
        }

        $_SESSION['output_message'] = 'Não foi possível atualizar o cliente!';
        $_SESSION['output_message_tipo'] = 'danger';
        return 0;
    }

    /**
     * Método para atualizar um destaque
     * @param $data
     * @param $id
     */
    public function update($data, $id)
    {
        $this->Formulario($data);
        if (empty($this->cliente->getNome())) {
            $_SESSION['output_message'] = 'Cliente não atualizado!<br/>Campos marcados com * são obrigatórios.';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;
        }

        if(empty($_FILES['imagem']['name'])) {
            $image = $this->show($id);
            $data['logo'] = $image['logo'];
            $this->Formulario($data);
            if($this->updateItem($this->cliente, $id)) {
                $_SESSION['output_message'] = 'Cliente atualizado com sucesso!';
                $_SESSION['output_message_tipo'] = 'success';
                Forms::unsetFormData('cliente');
                return 1;
            }

            $_SESSION['output_message'] = 'Não foi possível atualizar o cliente!';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;

        }
        
        return $this->updateImagem($data, $id);

    }

    /**
     * Deleta um serviço
     */
    public function delete($id)
    {
        $imagem = $this->show($id);

        if($this->deleteItem($id)) {
            //remove a imagem do servidor
            unlink($this->diretorioUpload.$imagem['logo']);

            $_SESSION['output_message'] = 'Cliente deletado com sucesso!';
            $_SESSION['output_message_tipo'] = 'success';
            return 1;
        } else {
            $_SESSION['output_message'] = 'Erro ao deletar o cliente!';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;
        }
    }

}