<?php
namespace Jcms\Core\Controllers;

use Jcms\Core\Models\Dao\ConteudoDAO;
use Jcms\Core\Models\Conteudo;
use Jcms\Core\Ext\Content;
use Intervention\Image\ImageManagerStatic as Image;
use Jcms\Core\Ext\Forms;

class DestaqueController extends ConteudoDAO
{
    private $conteudo;
    private $diretorioUpload; 

    public function __construct()
    {
        parent::__construct();
        $this->conteudo = new Conteudo();
        $this->diretorioUpload = 'public/uploaded_files/destaque/';
    }

    /**
     * Lista todos os destaques cadastrados no BD
     */
    public function listaTodosDestaquesPainel()
    {
        $this->addFilter(array("tipo","=",4));
        return $this->listItems(0,0, true);
    }

    /**
     * lista todos os destaques cadastrados
     */
    public function listaDestaqueHome($limite)
    {
        $this->addFilter(array("tipo","=",4));
        $this->addFilter(array("publicado","=",1));
        return $this->listItems(0, $limite, true);
    }


    /**
     * @param $id
     */
    public function show($id)
    {
        $this->addFilter(array('id', '=', $id));
        $data = $this->listItems(0,1,true);
        $this->Formulario($data[0]);
        return $data[0];
    }

    /**
     * 
     */
    public function Formulario($data) 
    {
        unset($_SESSION['formulario_destaque']);

        $this->conteudo->setUrl(Content::gerarURL($data['titulo']));
        $this->conteudo->setTitulo($data['titulo']);
        $this->conteudo->setTipo(4); // tipo 4 = destaque
        $this->conteudo->setResumo(isset($data['resumo']) ? $data['resumo'] : null);
        $this->conteudo->setCorpo(isset($data['corpo']) ? $data['corpo'] : null);
        $this->conteudo->setBanner(isset($data['banner']) ? $data['banner'] : null);
        $this->conteudo->setPublicado(isset($data['publicado']) ? $data['publicado'] : null);

        Forms::setFormData($data, 'destaque');
    }

    /**
     * cria um novo destaque
     */
    public function create($data) 
    {
        $temp = explode(".", $_FILES['imagem']['name']);
        $data['banner'] = time().uniqid(rand()).'.'.end($temp);
        $data['publicado'] = ($data['publicado'] == 'on') ? '1': '0';
        $this->Formulario($data);

        if (empty($this->conteudo->getTitulo()) || empty($_FILES['imagem']['name'])) {
            $_SESSION['output_message'] = "Destaque não cadastrado!<br/>Campos marcados com * são obrigatórios.";
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;
        }

        if($_FILES['imagem']['type'] != 'image/jpeg' && $_FILES['imagem']['type'] != 'image/png' && $_FILES['imagem']['type'] != 'image/gif') {
            $_SESSION['output_message'] = 'O formato da Imagem não é permitido';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;
        }

        if(!$this->insertItem($this->conteudo)) {
            $_SESSION['output_message'] = 'Não foi possível cadastrar o destaque!';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;
        }

        $upload_arq = $this->diretorioUpload.$data['banner'];

        if(Image::make($_FILES['imagem']['tmp_name'])->widen(1110)->save($upload_arq)) {
            $_SESSION['output_message'] = 'Destaque cadastrado com sucesso!';
            $_SESSION['output_message_tipo'] = 'success';
            Forms::unsetFormData('destaque');
            return 1;
        }

        $_SESSION['output_message'] = 'erro no upload da imagem!';
        $_SESSION['output_message_tipo'] = 'danger';
        return 0;
    }

    /**
     * 
     */
    public function updateImagem($data, $id) 
    {
        if($_FILES['imagem']['type'] != 'image/jpeg' &&  $_FILES['imagem']['type'] != 'image/png' &&  $_FILES['imagem']['type'] != 'image/gif') {

            $_SESSION['output_message'] = 'O formato da Imagem não é permitido';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;

        }

        $temp = explode(".", $_FILES['imagem']['name']);
        $data['banner'] = time().uniqid(rand()).'.'.end($temp);
        //remove a imagem do servidor
        $image = $this->show($id);
        unlink($this->diretorioUpload.$image['banner']);
        $data['publicado'] = ($data['publicado'] == 'on') ? '1': '0';

        $this->Formulario($data);

        if($this->updateItem($this->conteudo, $id)) {

            $upload_arq = $this->diretorioUpload.$data['banner'];

            if(Image::make($_FILES['imagem']['tmp_name'])->widen(1110)->save($upload_arq)) {
                $_SESSION['output_message'] = 'Destaque atualizado com sucesso!';
                $_SESSION['output_message_tipo'] = 'success';
                return 1;
            }

            $_SESSION['output_message'] = 'erro no upload da imagem!';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;

        }

        $_SESSION['output_message'] = 'Não foi possível atualizar o destaque!';
        $_SESSION['output_message_tipo'] = 'danger';
        return 0;
    }

    /**
     * Método para atualizar um destaque
     * @param $data
     * @param $id
     */
    public function update($data, $id)
    {
        $this->Formulario($data);

        if (empty($this->conteudo->getTitulo())) {
            $_SESSION['output_message'] = 'Destaque não atualizado!<br/>Campos marcados com * são obrigatórios.';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;
        }

        if(empty($_FILES['imagem']['name'])) {

            $image = $this->show($id);
            $data['banner'] = $image['banner'];
            $data['publicado'] = ($data['publicado'] == 'on') ? '1': '0';

            $this->Formulario($data);

            if($this->updateItem($this->conteudo, $id)) {
                $_SESSION['output_message'] = 'Destaque atualizado com sucesso!';
                $_SESSION['output_message_tipo'] = 'success';
                return 1;
            }

            $_SESSION['output_message'] = 'Não foi possível atualizar o destaque!';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;

        }

        return $this->updateImagem($data, $id);
    }

    /**
     * Deleta um destaque
     */
    public function delete($id)
    {
        $imagem = $this->show($id);

        if($this->deleteItem($id)) {
            //remove a imagem do servidor
            unlink($this->diretorioUpload.$imagem['banner']);

            $_SESSION['output_message'] = 'Destaque deletado com sucesso!';
            $_SESSION['output_message_tipo'] = 'success';
            return 1;
        } else {
            $_SESSION['output_message'] = 'Erro ao deletar o destaque!';
            $_SESSION['output_message_tipo'] = 'danger';
            return 0;
        }
    }

}