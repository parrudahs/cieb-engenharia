<?php

use PHPMailer\PHPMailer\PHPMailer;	

$mail = new PHPMailer;

$conf = new \Jcms\Core\Controllers\ConfiguracaoController();

$mail->isSMTP();

/***********************
	CONFIGURAÇÕES
*/
$mail->SMTPDebug = 0;
$mail->SMTPAuth = true;
$mail->SMTPSecure = EMAIL_SMTP_SECURE;
$mail->Host = EMAIL_SMTP_HOST;
$mail->Port = EMAIL_SMTP_PORT;
$mail->Username = EMAIL_SMTP_LOGIN;
$mail->Password = EMAIL_SMTP_PASS;
$mail->CharSet = 'UTF-8';

$mail->setFrom($_POST['email'], $_POST['nome']);
$mail->Subject = "Cieb Engenharia - Página de contato";
$mail->msgHTML("<strong>Nome</strong> : ".$_POST['nome']."<br><strong>Email</strong> : ".$_POST['email']."<br><strong>Mensagem</strong> : ".$_POST['mensagem']);
$mail->addAddress($conf->returnEmailContato()['value']);

if(!$mail->send()) {
    $_SESSION['output_message'] = 'Mail error: '.$mail->ErrorInfo;
    $_SESSION['output_message_tipo'] = 'danger';
    header('Location:'.BASE_URL.'contato');
} else {
    $_SESSION['output_message'] = 'Mensagem enviado com sucesso, em breve entraremos em contato!';
    $_SESSION['output_message_tipo'] = 'success';
    header('Location:'.BASE_URL.'contato');
}
