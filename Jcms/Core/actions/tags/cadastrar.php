<?php
$login = new Jcms\Core\Auth\Autenticacao();
if (!$login->verificaSessao())
    header('Location:' . BASE_URL . 'admin');

$cad = new Jcms\Core\Controllers\TagController();
if(isset($_POST['cadastrar'])){
    if($cad->create($_POST)) {
        header('Location:'.BASE_URL.'admin/lista-tags');
    } else {
        header('Location:'.BASE_URL.'admin/cadastro-tag');
    }
}

?>