<?php
$login = new Jcms\Core\Auth\Autenticacao();
if (!$login->verificaSessao())
    header('Location:' . BASE_URL . 'admin');

$del = new \Jcms\Core\Controllers\TagController();
if(isset($urls[2]) && $urls[2] == 'deletar'){
    if($del->delete($urls[3])) {
        header('Location:'.BASE_URL.'admin/lista-tags');
    } else {
        header('Location:'.BASE_URL.'admin/lista-tags');
    } 
}

?>