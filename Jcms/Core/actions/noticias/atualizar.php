<?php

$login = new Jcms\Core\Auth\Autenticacao();
if (!$login->verificaSessao())
    header('Location:' . BASE_URL . 'admin');


$att = new Jcms\Core\Controllers\NoticiaController();
if(isset($_POST['atualizar'])){
    if($att->update($_POST, $urls[3])) {
        header('Location:'.BASE_URL.'admin/lista-noticias');
    } else {
        header('Location:'.BASE_URL.'admin/atualiza-noticia/'.$urls[3]);
    }
}

?>