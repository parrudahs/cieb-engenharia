<?php

$login = new Jcms\Core\Auth\Autenticacao();
if (!$login->verificaSessao())
    header('Location:' . BASE_URL . 'admin');


$att = new Jcms\Core\Controllers\ServicoController();

if(isset($_POST['atualizar'])){
    if($att->update($_POST, $urls[3])) {
        header('Location:'.BASE_URL.'admin/lista-servicos');
    } else {
        header('Location:'.BASE_URL.'admin/atualiza-servico/'.$urls[3]);
    }
}

?>