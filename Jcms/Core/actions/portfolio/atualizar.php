<?php

$login = new Jcms\Core\Auth\Autenticacao();
if (!$login->verificaSessao())
    header('Location:' . BASE_URL . 'admin');

$att = new Jcms\Core\Controllers\PortfolioController();
$id = $urls[3];
if(isset($_POST['atualizar'])){
    if($att->update($_POST, $id)) {
        header('Location:'.BASE_URL.'admin/lista-portfolio');
    } else {
        header('Location:'.BASE_URL.'admin/atualiza-portfolio/'.$id);
    }
}
?>