<?php

namespace Jcms\Core\Models;

class Configuracao
{

    private $configID;
    private $name;
    private $value;

    /*
        getters and setters
    */

    public function __construct($id=null) {
        if (!empty($id))
            $this->configID=$id;
    }

    public function getConfigID() { return $this->configID; }
    public function getName() { return $this->name; }
    public function getValue() { return $this->value; }

    public function setConfigID($configID) { $this->configID=$configID; }
    public function setName($name) { $this->name=$name; }
    public function setValue($value) { $this->value=$value; }
}

?>