<?php

namespace Jcms\Core\Models;

class Cliente
{

    private $nome;
    private $logo;
    private $descricao;
    private $site;
    private $data;

    /*
        getters and setters
    */
    public function getNome() { return $this->nome; }
    public function getLogo() { return $this->logo; }
    public function getDescricao() { return $this->descricao; }
    public function getSite() { return $this->site; }
    public function getData() { return $this->data; }

    public function setNome($nome) { $this->nome=$nome; }
    public function setLogo($logo) { $this->logo=$logo; }
    public function setDescricao($descricao) { $this->descricao=$descricao; }
    public function setSite($site) { $this->site=$site; }
    public function setData($data) { $this->data=$data; }

}

?>
