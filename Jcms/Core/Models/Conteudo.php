<?php

namespace Jcms\Core\Models;

class Conteudo {

	private $ID;
	private $titulo;
    private $banner;
	private $tipo;
	private $corpo;
    private $resumo;
    private $url;
    private $data;
    private $publicado;


	public function getID() { return $this->ID; }
	public function getTitulo() { return $this->titulo; }
    public function getBanner() { return $this->banner; }
	public function getTipo() { return $this->tipo; }
	public function getCorpo() { return $this->corpo; }
	public function getResumo() { return $this->resumo; }
	public function getUrl() { return $this->url; }
    public function getData() { return $this->data; }
    public function getPublicado() { return $this->publicado; }

	public function setID($ID) { $this->ID=$ID; }
	public function setTitulo($titulo) { $this->titulo=$titulo; }
    public function setBanner($banner) { $this->banner=$banner; }
	public function setTipo($tipo) { $this->tipo=$tipo; }
	public function setCorpo($corpo) { $this->corpo=$corpo; }
	public function setResumo($resumo) { $this->resumo=$resumo; }
	public function setUrl($url) { $this->url=$url; }
	public function setData($data) { $this->data=$data; }
	public function setPublicado($publicado) { $this->publicado=$publicado; }

}