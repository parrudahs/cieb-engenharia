<?php

namespace Jcms\Core\Models;

class Tag
{

    private $id;
    private $nome;

    /*
        getters and setters
    */

    public function __construct($id=null) {
        if (!empty($id))
            $this->id=$id;
    }

    public function getID() { return $this->ID; }
    public function getNome() { return $this->nome; }

    public function setConfigID($ID) { $this->ID=$ID; }
    public function setNome($nome) { $this->nome=$nome; }
}

?>