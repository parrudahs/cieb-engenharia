<?php

namespace Jcms\Core\Models\Dao;

use Jcms\Core\Models\Dao\DAO;
use Jcms\Core\Models\Tag;

class TagDAO extends DAO
{
	private $tbName = "tb_tag";
    private $tableID = "id";
    private $attributes = array();

    public function __construct()
    {
        $this->setTableName($this->tbName);
    }

    /**
     * Método para inserir uma nova categoria
     * @param Categoria $data
     * @return int
     */
	public function insertItem(Tag $data)
    {
        $this->attributes['nome'] = $data->getNome();

        return $this->insertDAO($this->attributes);
	}

    /**
     * Método para atualizar a categoria
     * @param Categoria $data
     * @param $id
     * @return int
     */
	public function updateItem(Tag $data, $id)
    {
        $this->attributes['nome'] = $data->getNome();

        return $this->updateDAO($this->attributes, $this->tableID, $id);
	}

    /**
     * Método para deletar Categoria
     * @param $id
     * @return int|mixed
     */
	public function deleteItem($id)
    {
        return $this->deleteDAO($this->tableID, $id);
	}


}

?>