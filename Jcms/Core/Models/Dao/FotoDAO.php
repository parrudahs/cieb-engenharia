<?php

namespace Jcms\Core\Models\Dao;

use Jcms\Core\Models\Foto;

class FotoDAO extends DAO
{

    private $tbName="tb_foto";
    private $tableID = "id";
    private $attributes = array();


    public function __construct()
    {
        $this->setTableName($this->tbName);
    }

    /**
     * @param Foto $data
     * @return int
     */
    public function insertItem(Foto $data)
    {
        $this->attributes['descricao'] =  $data->getDescricao();
        $this->attributes['imagem'] = $data->getImagem();
        $this->attributes['publicar'] = $data->getPublicar();
        $this->attributes['portfolio_id'] = $data->getPortfolioId();
        $this->attributes['capa'] = $data->getCapa();

        return $this->insertDAO($this->attributes);
    }

    /**
     * @param Foto $data
     * @param $id
     * @return int
     */
    public function updateItem(Foto $data, $id)
    {
        $this->attributes['descricao'] =  $data->getDescricao();
        $this->attributes['imagem'] = $data->getImagem();
        $this->attributes['publicar'] = $data->getPublicar();
        $this->attributes['portfolio_id'] = $data->getPortfolioId();
        $this->attributes['capa'] = $data->getCapa();

        return $this->updateDAO($this->attributes, $this->tableID, $id);
    }

    /**
     * @param $id
     * @return int
     */
    public function deleteItem($id)
    {
        return $this->deleteDAO($this->tableID, $id);
    }

    /**
     * @param $id
     * @return int
    */
    public function deleteAllFotosPortfolio($id)
    {
        return $this->deleteDAO('portfolio_id', $id);
    }


}    

?>