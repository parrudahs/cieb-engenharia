<?php

namespace Jcms\Core\Models\Dao;

use Jcms\Core\Models\Database;
use Jcms\Core\Models\Destaque;

abstract class DAO extends Database {


    private $tableName;
    public $sorterBy = array();
    public $filterBy = array();    
    private $rowCount=0;

    /**
     * @param $sorterBy
     * @param $filterBy
     * @param $rowIndexIni
     * @param $rowIndexEnd
     * @param null $where_clause
     * @param bool $rowCount
     * @return array|int|mixed
     */
    public function listDAO($sorterBy, $filterBy, $rowIndexIni, $rowIndexEnd, $rowCount = null, $where_clause = null) 
    {
        try {
            $sorter = $this->sorter($sorterBy);

            if (empty($where_clause))
                $filter = $this->filtros($filterBy);
            else
                $filter = $where_clause;

            $rowIndexIni = (int) $rowIndexIni;
            $rowIndexEnd = (int) $rowIndexEnd;

            if (isset($rowIndexEnd) && $rowIndexEnd > 0)
                $limit = "LIMIT " . $rowIndexIni . "," . $rowIndexEnd;

            $sql = "SELECT * FROM " . $this->tableName . " " . (!empty($filter) ? $filter : null) . " " . (!empty($sorter) ? $sorter : null) . " " . (!empty($limit) ? $limit : null);
            
            //echo $sql;

            $stmt = parent::conn()->prepare($sql);

            foreach ($filterBy as $f) {
                $stmt->bindValue(":" . $f[0], $f[2]);
            }

            $stmt->execute();

            if ($rowCount == true)
                return $stmt->rowCount();

            return $stmt->fetchAll(\PDO::FETCH_ASSOC);

        } catch (\PDOException $e) {
            die("Erro ao selecionar : " . $e->getMessage());
        }
    }

    /**
     * Método de Inserção do DAO
     * @param array $dados
     * @return int
     */
    public function insertDAO(array $dados) 
    {
        try {
            $sql = "INSERT INTO " . $this->tableName;
            $colunas = '';
            $values = '';
            foreach ($dados as $k => $v) {
                $colunas .= $k . ",";
                if ($v == 'unix_timestamp')
                    $values .= "UNIX_TIMESTAMP(),";
                else
                    $values .= ":" . $k . ",";
            }
            $colunas = substr($colunas, 0, -1);
            $values = substr($values, 0, -1);

            $sql .= " (" . $colunas . ") VALUES( " . $values . ")";
            //echo $sql;
            $stmt = parent::conn()->prepare($sql);
            foreach ($dados as $k => $v) {
                if ($v != 'unix_timestamp')
                    $stmt->bindValue(":" . $k, $v);
            }
            if ($stmt->execute())
                return 1;
        } catch (\PDOException $e) {
            die("erro:" . $e->getMessage());
        }
        return 0;
    }

    /**
     * Método de update do DAO
     * @param array $dados
     * @param $campo
     * @param $id
     * @return int
     */
    public function updateDAO(array $dados, $campo, $id) 
    {
        try {
            $colunas = '';
            foreach ($dados as $k => $v) {
                $colunas .= $k . "= :" . $k . ",";
            }
            $colunas = substr($colunas, 0, -1);
            $sql = "UPDATE " . $this->tableName . " SET " . $colunas . " WHERE " . $campo . "= :" . $campo;
            $stmt = parent::conn()->prepare($sql);
            foreach ($dados as $k => $v) {
                if ($v != 'unix_timestamp')
                    $stmt->bindValue(":" . $k, $v);
            }
            $stmt->bindValue(":" . $campo, $id);
            if ($stmt->execute())
                return 1;
        } catch (\PDOException $e) {
            die("erro:" . $e->getMessage());
        }
        return 0;
    }

    /**
     * Método de delete do DAO
     * @param $campo
     * @param $value
     * @return int
     */
    public function deleteDAO($campo, $value) 
    {
        try {
            if (!empty($campo) && !empty($value)) {
                $sql = "DELETE FROM " . $this->tableName . " WHERE " . $campo . " = ? ";
                $stmt = parent::conn()->prepare($sql);
                $stmt->bindParam(1, $value);
                echo $sql;
                if ($stmt->execute())
                    return 1;
            }
            return 0;
        } catch (\PDOException $e) {
            die("erro:" . $e->getMessage());
        }
    }

     /**
     * @param $rowIndexIni
     * @param $rowIndexEnd
     * @return mixed
     */
    public function listItems($rowIndexIni, $rowIndexEnd, $rowCount=null)
    {
        if($rowCount == true)
            $this->setRowCount($this->listDAO($this->sorterBy, $this->filterBy, $rowIndexIni, $rowIndexEnd, $rowCount));
        return $this->listDAO($this->sorterBy, $this->filterBy, $rowIndexIni, $rowIndexEnd);
    }  


    /**
     * 
     */
    private function sorter($array) 
    {
        $sorter = null;
        if (is_array($array) && !empty($array)) {
            $sorter = "ORDER BY ";
            $num = sizeof($array);
            foreach ($array as $k => $s) {
                if ($k == ($num - 1))
                    $sorter .= $s[0] . " " . $s[1] . " ";
                else
                    $sorter .= $s[0] . " " . $s[1] . ", ";
            }
        }
        return $sorter;
    }

    /**
     * 
     */
    private function filtros($array) 
    {
        if (is_array($array) && !empty($array)) {
            $filter = " WHERE 1";
            foreach ($array as $f) {
                $filter .= " AND " . $f[0] . " " . $f[1] . " :" . $f[0];
            }
            return $filter;
        }
        return null;
    }

    /**
     * @param $param
     */
    public function addSorter($param)
    {
        array_push($this->sorterBy,$param);
    }

    /**
     * @param $param
     */
    public function addFilter($param)
    {
        array_push($this->filterBy,$param);
    }

    /**
     * @param $param
    */
    public function clearFilter()
    {
        $this->filterBy = array();
    }

    /**
     * @param $param
    */
    public function clearSorter()
    {
        $this->sorterBy = array();
    }


    /**
     * @return mixed
     */
    public function getRowCount()
    {
        return $this->rowCount;
    }

    /**
     * @param mixed $rowCount
     */
    protected function setRowCount($rowCount)
    {
        $this->rowCount = $rowCount;
    }    

    /**
     * 
     */
    protected function setTableName($name) 
    {
        $this->tableName = $name;
    }

    /**
     * 
     */
    protected function getTableName() {
        return $this->tableName;
    }
}
