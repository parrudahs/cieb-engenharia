<?php

namespace Jcms\Core\Models\Dao;

use Jcms\Core\Models\Portfolio;

class PortfolioDAO extends DAO
{

    private $tbName = "tb_portfolio";
    private $tableID = "id";
    private $attributes = array();

    public function __construct()
    {
        $this->setTableName($this->tbName);
    }

    /**
     * @param Galeria $data
     * @return int
     */
	public function insertItem(Portfolio $data)
    {
        $this->attributes['nome']       = $data->getNome();
        $this->attributes['local']      =  $data->getLocal();
        $this->attributes['estado']     = $data->getEstado();
        $this->attributes['sigla']      = $data->getSigla();
        $this->attributes['descricao']  = $data->getDescricao();
        $this->attributes['tag_id']     = $data->getTagId();
        $this->attributes['publicado']  = $data->getPublicado();
        $this->attributes['data']       = date('Y-m-d H:i:s');


        return $this->insertDAO($this->attributes);

	}

    /**
     * @param Galeria $data
     * @param $id
     * @return int
     */
	public function updateItem(Portfolio $data, $id)
    {
        $this->attributes['nome'] = $data->getNome();
        $this->attributes['local'] =  $data->getLocal();
        $this->attributes['estado'] = $data->getEstado();
        $this->attributes['sigla'] = $data->getSigla();
        $this->attributes['descricao'] = $data->getDescricao();
        $this->attributes['tag_id'] = $data->getTagId();
        $this->attributes['publicado'] = $data->getPublicado();
        
        return $this->updateDAO($this->attributes, $this->tableID, $id);
	}

    /**
     * deleta galeria
     * @param $id
     * @return int
     */
	public function deleteItem($id)
    {
        return $this->deleteDAO($this->tableID, $id);
	}

}


?>