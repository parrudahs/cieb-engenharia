<?php

namespace Jcms\Core\Models\Dao;

use Jcms\Core\Models\Dao\DAO;
use Jcms\Core\Models\Cliente;

class ClienteDAO extends DAO
{
	private $tbName = "tb_cliente";
    private $tableID = "id";
    private $attributes = array();

    public function __construct()
    {
        $this->setTableName($this->tbName);
    }

    /**
     * Método para inserir uma nova categoria
     * @param Categoria $data
     * @return int
     */
	public function insertItem(Cliente $data)
    {
        $this->attributes['nome'] = $data->getNome();
        $this->attributes['logo'] =  $data->getLogo();
        $this->attributes['descricao'] = $data->getDescricao();
        $this->attributes['site'] = $data->getSite();
        $this->attributes['data'] = $data->getData();

        return $this->insertDAO($this->attributes);
	}

    /**
     * Método para atualizar a categoria
     * @param Categoria $data
     * @param $id
     * @return int
     */
	public function updateItem(Cliente $data, $id)
    {
        $this->attributes['nome'] = $data->getNome();
        $this->attributes['logo'] =  $data->getLogo();
        $this->attributes['descricao'] = $data->getDescricao();
        $this->attributes['site'] = $data->getSite();
        $this->attributes['data'] = $data->getData();

        return $this->updateDAO($this->attributes, $this->tableID, $id);
	}

    /**
     * Método para deletar Categoria
     * @param $id
     * @return int|mixed
     */
	public function deleteItem($id)
    {
        return $this->deleteDAO($this->tableID, $id);
	}


}

?>