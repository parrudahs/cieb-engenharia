<?php

namespace Jcms\Core\Models\Dao;

use Jcms\Core\Models\Dao\DAO;
use Jcms\Core\Models\Conteudo;

class ConteudoDAO extends DAO
{

    private $tbName = "tb_conteudo";
    private $tableID = "id";
    private $attributes = array();

    public function __construct()
    {
        $this->setTableName($this->tbName);
        parent::__construct();
    }

    /**
     * @param array $data
     * @return int
     */
    public function insertItem(Conteudo $data)
    {
        $this->attributes['titulo'] = $data->getTitulo();
        $this->attributes['banner'] =  $data->getBanner();
        $this->attributes['tipo'] = $data->getTipo();
        $this->attributes['corpo'] = $data->getCorpo();
        $this->attributes['resumo'] = $data->getResumo();
        $this->attributes['url'] = $data->getUrl();
        $this->attributes['data'] = date('Y-m-d H:i:s');
        $this->attributes['publicado'] = $data->getPublicado();

        return $this->insertDAO($this->attributes);
    }

    /**
     * @param array $data
     * @param $id
     * @return int
     */
    public function updateItem(Conteudo $data, $id)
    {

        $this->attributes['titulo'] = $data->getTitulo();
        $this->attributes['banner'] =  $data->getBanner();
        $this->attributes['tipo'] = $data->getTipo();
        $this->attributes['corpo'] = $data->getCorpo();
        $this->attributes['resumo'] = $data->getResumo();
        $this->attributes['url'] = $data->getUrl();
        $this->attributes['publicado'] = $data->getPublicado();

        return $this->updateDAO($this->attributes, $this->tableID, $id);
    }

    /**
     * @param $id
     * @return int
     */
    public function deleteItem($id)
    {
        return $this->deleteDAO($this->tableID, $id);
    }
    
}