<?php

namespace Jcms\Core\Models\Dao;

use Jcms\Core\Models\Configuracao;
use Jcms\Core\Models\Dao\DAO;

class ConfiguracoesDAO extends DAO
{

    private $tbName = "tb_configuracoes";
    private $tableID = "config_id";
    private $attributes = array();

    public function __construct()
    {
        $this->setTableName($this->tbName);
    }

    /**
     * Método para inserção
     * @param Configuracao $data
     * @return mixed
     */
    public function insertItem(Configuracao $data) 
    {
        $this->attributes['name'] = $data->getName();
        $this->attributes['value'] =  $data->getValue();
    
        return $this->insertDAO($this->attributes);
    }

    /**
     * @param Configuracao $data
     * @param $id
     * @return mixed
     */
    public function updateItem(Configuracao $data, $id)
    {
        $this->attributes['name'] = $data->getName();
        $this->attributes['value'] =  $data->getValue();

        return $this->updateDAO($this->attributes, $this->tableID, $id);
    }

    /**
     * @param $id
     * @return int
     */
    public function deleteItem($id)
    {
        return $this->deleteDAO($this->tableID, $id);
    }

}    
    

?>