<?php

namespace Jcms\Core\Models;

class Portfolio
{

    private $id;
    private $nome;
    private $local;
    private $estado;
    private $sigla;
    private $tagId;
    private $descricao;
    private $data;
    private $publicado;

    /*
        getters and setters
    */
    public function getId() { return $this->id; }
    public function getNome() { return $this->nome; }
    public function getLocal() { return $this->local; }
    public function getEstado() { return $this->estado; }
    public function getSigla() { return $this->sigla; }
    public function getTagId() { return $this->tagId; }
    public function getData() { return $this->data; }
    public function getDescricao() { return $this->descricao; }
    public function getPublicado() { return $this->publicado; }

    public function setId($id) { $this->id=$id; }
    public function setNome($nome) { $this->nome=$nome; }
    public function setLocal($local) { $this->local=$local; }
    public function setEstado($estado) { $this->estado=$estado; }
    public function setSigla($sigla) { $this->sigla=$sigla; }
    public function setTagId($tagId) { $this->tagId=$tagId; }
    public function setData($data) { $this->data=$data; }
    public function setDescricao($descricao) { $this->descricao=$descricao; }
    public function setPublicado($publicado) { $this->publicado=$publicado; }

}

?>
