<?php

namespace Jcms\Core\Models;

class Foto {

	private $id;
    private $descricao;
    private $imagem;
    private $publicar;
    private $portfolioId;
    private $capa;

    /*
        getters and setters
    */

	public function getId() { return $this->id; }
	public function getDescricao() { return $this->descricao; }
	public function getImagem() { return $this->imagem; }
    public function getPublicar() { return $this->publicar; }
    public function getPortfolioId() { return $this->portfolioId; }
    public function getCapa() { return $this->capa; }

	public function setId($id) { $this->id=$id; }
	public function setDescricao($descricao) { $this->descricao=$descricao; }
	public function setImagem($imagem) { $this->imagem=$imagem; }
    public function setPublicar($publicar) { $this->publicar=$publicar; }
    public function setPortfolioId($portfolioId) { $this->portfolioId=$portfolioId; }
    public function setCapa($capa) { $this->capa=$capa; }
    
   }

?>