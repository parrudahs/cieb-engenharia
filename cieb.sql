-- MySQL dump 10.13  Distrib 5.7.19, for Linux (x86_64)
--
-- Host: localhost    Database: cieb
-- ------------------------------------------------------
-- Server version	5.7.19-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tb_cliente`
--

DROP TABLE IF EXISTS `tb_cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_cliente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `descricao` varchar(255) DEFAULT NULL,
  `site` varchar(255) DEFAULT NULL,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_cliente`
--

LOCK TABLES `tb_cliente` WRITE;
/*!40000 ALTER TABLE `tb_cliente` DISABLE KEYS */;
INSERT INTO `tb_cliente` VALUES (3,'Banco de Brasil','1508983857158451601659f144319e724.png','<p>descricao</p>\r\n','http://www.bb.com.br/pbb/pagina-inicial','2017-10-26 02:10:57'),(4,'Banco de Brasil','1508984919190012036459f1485779866.jpg','<p>descricao</p>\r\n','http://www.bb.com.br/pbb/pagina-inicial','2017-10-26 02:28:39'),(5,'Banco de Brasil','151014614118532894875a03005da4aba.png','<p>descricao</p>\r\n','http://www.bb.com.br/pbb/pagina-inicial','2017-11-08 13:02:21');
/*!40000 ALTER TABLE `tb_cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_configuracoes`
--

DROP TABLE IF EXISTS `tb_configuracoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_configuracoes` (
  `config_id` smallint(4) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(60) DEFAULT NULL,
  `value` tinytext,
  PRIMARY KEY (`config_id`),
  KEY `name` (`name`(30))
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_configuracoes`
--

LOCK TABLES `tb_configuracoes` WRITE;
/*!40000 ALTER TABLE `tb_configuracoes` DISABLE KEYS */;
INSERT INTO `tb_configuracoes` VALUES (1,'PASSWORD_CMS','202cb962ac59075b964b07152d234b70'),(2,'EMAILS_CONTATO','edson@aggiltransportes.com.br');
/*!40000 ALTER TABLE `tb_configuracoes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_conteudo`
--

DROP TABLE IF EXISTS `tb_conteudo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_conteudo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) NOT NULL,
  `banner` varchar(255) NOT NULL,
  `tipo` smallint(2) NOT NULL,
  `corpo` text NOT NULL,
  `resumo` varchar(255) DEFAULT NULL,
  `url` varchar(255) NOT NULL,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `publicado` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_conteudo`
--

LOCK TABLES `tb_conteudo` WRITE;
/*!40000 ALTER TABLE `tb_conteudo` DISABLE KEYS */;
INSERT INTO `tb_conteudo` VALUES (1,'Institucional','1508659728153605767159ec5210b2ae4.jpg',1,'<p>Desde 1984, nos destacamos por obras com qualidade, pontualidade e pelo bom relacionamento com nossos clientes.</p>\r\n\r\n<p>A Cieb Engenharia atua a 28 anos por todo o Brasil, especialmente no Distrito Federal e Goi&aacute;s. O uso de materiais e equipamentos de qualidade e alta tecnologia, equipe qualificada, juntamente com controle de qualidade durante todo processo construtivo, evidenciam o comprometimento da Cieb em oferecer obras com excel&ecirc;ncia.</p>\r\n\r\n<p>Sua vasta experi&ecirc;ncia possibilita ao cliente o conforto e seguran&ccedil;a de ter em sua obra uma empresa comprometida em oferecer acabamentos bem executados, instala&ccedil;&otilde;es seguras e entrega dentro do prazo.</p>\r\n\r\n<h3>Nossos Clientes</h3>\r\n\r\n<p>Desde 1984, nos destacamos por obras com qualidade, pontualidade e pelo bom relacionamento com nossos clientes.</p>\r\n\r\n<p>A Cieb Engenharia atua a 28 anos por todo o Brasil, especialmente no Distrito Federal e Goi&aacute;s. O uso de materiais e equipamentos de qualidade e alta tecnologia, equipe qualificada, juntamente com controle de qualidade durante todo processo construtivo, evidenciam o comprometimento da Cieb em oferecer obras com excel&ecirc;ncia.</p>\r\n\r\n<p>Sua vasta experi&ecirc;ncia possibilita ao cliente o conforto e seguran&ccedil;a de ter em sua obra uma empresa comprometida em oferecer acabamentos bem executados, instala&ccedil;&otilde;es seguras e entrega dentro do prazo.</p>\r\n','<p>A Cieb Engenharia atua a 28 anos por todo o Brasil, especialmente no Distrito Federal e Goi&aacute;s. O uso de materiais e equipamentos de qualidade e alta tecnologia, equipe qualificada.</p>\r\n','institucional','2017-10-22 08:08:48',NULL),(7,'Obras Hospitalares','1508810198168694765159ee9dd6b6530.jpg',2,'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean et pellentesque erat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed in erat vel urna commodo ullamcorper id in libero. Etiam eu mi dolor. Sed volutpat lorem vel lectus fringilla, tincidunt cursus est tincidunt. Cras egestas fermentum ipsum in bibendum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Fusce non est suscipit, mattis velit ac, lacinia erat. Nulla malesuada dolor orci, vel feugiat enim pretium quis. Etiam vulputate accumsan massa et euismod. Phasellus venenatis erat et euismod pellentesque. Nulla dui urna, fringilla sed placerat at, rhoncus eget eros.</p>\r\n\r\n<h3>Lorem ipsum dolor sit amet</h3>\r\n\r\n<p>Quisque gravida condimentum turpis. Morbi at ultricies lacus. Integer egestas libero eget augue luctus, vel blandit est molestie. Fusce faucibus dolor justo, aliquet mollis ante lobortis quis. Donec sagittis volutpat condimentum. Cras eu risus dui. Quisque vitae lorem sem. Mauris imperdiet diam vitae justo ornare, ut scelerisque mi tincidunt. Nam vitae magna dolor. Sed eget scelerisque nisl, convallis iaculis justo. Nulla tempor vitae ipsum ac tristique.</p>\r\n','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean et pellentesque erat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>\r\n','obras-hospitalares','2017-11-08 13:02:46',1),(8,'Nova Noticia de Testte','151014615517119781235a03006bae7b6.png',3,'<p>teste</p>\r\n','<p>teste</p>\r\n','nova-noticia-de-testte','2017-11-08 14:02:35',1),(9,'teste destaque','151014618712054088115a03008bde696.png',3,'<p>asdasd</p>\r\n','<p>adasd</p>\r\n','teste-destaque','2017-11-08 14:03:07',1),(10,'destaque','1510146216252630915a0300a89d5a3.png',4,'<p>teste</p>\r\n','<p>teste</p>\r\n','destaque','2017-11-08 14:03:36',1);
/*!40000 ALTER TABLE `tb_conteudo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_foto`
--

DROP TABLE IF EXISTS `tb_foto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_foto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(255) DEFAULT NULL,
  `imagem` varchar(45) NOT NULL,
  `publicar` tinyint(1) DEFAULT NULL,
  `portfolio_id` int(11) NOT NULL,
  `capa` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tb_foto_1_idx` (`portfolio_id`),
  CONSTRAINT `fk_tb_foto_1` FOREIGN KEY (`portfolio_id`) REFERENCES `tb_portfolio` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_foto`
--

LOCK TABLES `tb_foto` WRITE;
/*!40000 ALTER TABLE `tb_foto` DISABLE KEYS */;
INSERT INTO `tb_foto` VALUES (1,'','151014632014153184825a03011039bc7.png',1,1,1),(2,NULL,'151014634315969662875a0301271f74c.png',1,1,0),(3,NULL,'15101463437991752315a030127b5f0e.png',1,1,0),(4,NULL,'1510146344318885455a030128422c4.png',1,1,0),(5,NULL,'151014634415794184025a030128d4130.png',1,1,0),(6,NULL,'151014634515542109195a030129621ee.png',1,1,0);
/*!40000 ALTER TABLE `tb_foto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_portfolio`
--

DROP TABLE IF EXISTS `tb_portfolio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_portfolio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `local` varchar(100) NOT NULL,
  `estado` varchar(100) NOT NULL,
  `sigla` varchar(3) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `publicado` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tb_portfolio_1_idx` (`tag_id`),
  CONSTRAINT `fk_tb_portfolio_1` FOREIGN KEY (`tag_id`) REFERENCES `tb_tag` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_portfolio`
--

LOCK TABLES `tb_portfolio` WRITE;
/*!40000 ALTER TABLE `tb_portfolio` DISABLE KEYS */;
INSERT INTO `tb_portfolio` VALUES (1,'Um novo produto 4 5','POCONÃ‰','Mato Grosso','mt',1,'2017-11-08 14:00:35',0);
/*!40000 ALTER TABLE `tb_portfolio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_tag`
--

DROP TABLE IF EXISTS `tb_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_tag`
--

LOCK TABLES `tb_tag` WRITE;
/*!40000 ALTER TABLE `tb_tag` DISABLE KEYS */;
INSERT INTO `tb_tag` VALUES (1,'Teste');
/*!40000 ALTER TABLE `tb_tag` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-08 10:06:32
