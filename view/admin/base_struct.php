<?php

use \Jcms\Core\Ext\Forms;

$login = new Jcms\Core\Auth\Autenticacao();

if (isset($urls[2]) && $urls[2] == "sair") {
    if ($login->logoff())
        header('Location:' . BASE_URL . 'admin');
}

if (!$login->verificaSessao())
    header('Location:' . BASE_URL . 'admin');

ob_start();

/**
 * Matriz com o nome e as URI do sistema.
 */
$urlMap = [
    'cliente' => [
        'listar' => '',
        'cadastrar' => '',
        'atualizar' => ''
    ],
    'portifolio' => [
        'listar' => '',
        'cadastrar' => '',
        'atualizar' => ''
    ],
    'tag' => [
        'listar' => '',
        'cadastrar' => '',
        'atualizar' => ''
    ],
    'institucional' => [
        'listar' => '',
        'cadastrar' => '',
        'atualizar' => ''
    ],
]

?>
    <!DOCTYPE html>
    <html lang="pt-BR">
    <head>
        <title>:: CIEB Engenharia ::</title>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Baloo" rel="stylesheet"/>
        <!-- Font Awesome 4.7.0 -->
        <link rel="stylesheet" href="<?= BASE_URL ?>/public/libs/font-awesome/css/font-awesome.css"/>
        <!-- Bootstrap v3.3.7 -->
        <link rel="stylesheet" href="<?= BASE_URL ?>/public/libs/bootstrap/css/bootstrap.css"/>


        <link rel="stylesheet" href="<?= BASE_URL ?>/public/css/default.css"/>

        <!-- BOOTSTRAP THEME -->
        <!--<link rel="stylesheet" href="<? /*= BASE_URL */ ?>/public/libs/bootstrap/css/pingo-theme.css"/>-->


        <!-- Data tables -->
        <link rel="stylesheet" href="<?= BASE_URL ?>/public/libs/data-tables/css/dataTables.bootstrap.css"/>

        <link rel="stylesheet" href="<?= BASE_URL ?>/public/libs/dropzone/dropzone.css"/>
    </head>
    <body>
    <!-- REQUEST URI  -->
    <?php $uri = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : ''; ?>
    <!-- MENU SUPERIOR -->
    <nav class="animated fadeIn navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?= BASE_URL ?>admin/inicial">
                    <b style="color: #355e5e">CIEB</b> Engenharia
                </a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <!-- OPÇÕES PARA DESTAQUE -->
                    <li class="dropdown<?= strpos($uri, 'destaque') ? ' active' : ''; ?>">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">
                            <i class="fa fa-1p3x fa-tv"></i>
                            &nbsp;&nbsp;Destaques
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="<?= BASE_URL ?>admin/cadastro-destaque">
                                    <i class="fa fa-plus"></i> &nbsp;&nbsp;&nbsp;
                                    Adicionar destaque
                                </a>
                            </li>
                            <li>
                                <a href="<?= BASE_URL ?>admin/lista-destaques">
                                    <i class="fa fa-reorder"></i> &nbsp;&nbsp;&nbsp;
                                    Listar destaques
                                </a>
                            </li>
                        </ul>
                    </li>
                    <!-- OPÇÕES PARA SERVIÇOS -->
                    <li class="dropdown<?= strpos($uri, 'servico') ? ' active' : ''; ?>">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">
                            <i class="fa fa-1p3x fa-briefcase"></i>
                            &nbsp;&nbsp;Serviços
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="<?= BASE_URL ?>admin/cadastro-servico">
                                    <i class="fa fa-plus"></i> &nbsp;&nbsp;&nbsp;
                                    Adicionar serviço
                                </a>
                            </li>
                            <li>
                                <a href="<?= BASE_URL ?>admin/lista-servicos">
                                    <i class="fa fa-reorder"></i> &nbsp;&nbsp;&nbsp;
                                    Listar serviços
                                </a>
                            </li>
                        </ul>
                    </li>
                    <!-- OPÇÕES PARA NOTÍCIAS -->
                    <li class="dropdown<?= strpos($uri, 'noticia') ? ' active' : ''; ?>">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">
                            <i class="fa fa-1p3x fa-newspaper-o"></i>
                            &nbsp;&nbsp;Notícias
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="<?= BASE_URL ?>admin/cadastro-noticia">
                                    <i class="fa fa-plus"></i> &nbsp;&nbsp;&nbsp;
                                    Adicionar notícia
                                </a>
                            </li>
                            <li>
                                <a href="<?= BASE_URL ?>admin/lista-noticias">
                                    <i class="fa fa-reorder"></i> &nbsp;&nbsp;&nbsp;
                                    Listar notícias
                                </a>
                            </li>
                        </ul>
                    </li>
                    <!-- OPÇÕES PARA CLIENTES -->
                    <li class="dropdown hidden-sm<?= strpos($uri, 'cliente') ? ' active' : ''; ?>">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">
                            <i class="fa fa-1p3x fa-user"></i>
                            &nbsp;&nbsp;Clientes
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="<?= BASE_URL ?>admin/cadastro-cliente">
                                    <i class="fa fa-plus"></i> &nbsp;&nbsp;&nbsp;
                                    Adicionar cliente
                                </a>
                            </li>
                            <li>
                                <a href="<?= BASE_URL ?>admin/lista-clientes">
                                    <i class="fa fa-reorder"></i> &nbsp;&nbsp;&nbsp;
                                    Listar clientes
                                </a>
                            </li>
                        </ul>
                    </li>
                    <!-- OPÇÕES PARA PORTIFÓLIO -->
                    <li class="dropdown hidden-sm<?= strpos($uri, 'portfolio') ? ' active' : ''; ?>">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">
                            <i class="fa fa-1p3x fa-photo"></i>
                            &nbsp;&nbsp;Portfólio
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="<?= BASE_URL ?>admin/cadastro-portfolio">
                                    <i class="fa fa-plus"></i> &nbsp;&nbsp;&nbsp;
                                    Adicionar trabalho
                                </a>
                            </li>
                            <li>
                                <a href="<?= BASE_URL ?>admin/lista-portfolio">
                                    <i class="fa fa-reorder"></i> &nbsp;&nbsp;&nbsp;
                                    Listar trabalhos
                                </a>
                            </li>
                        </ul>
                    </li>
                    <!-- OPÇÕES PARA TAGS -->
                    <li class="dropdown hidden-sm hidden-md<?= strpos($uri, 'tag') ? ' active' : ''; ?>">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">
                            <i class="fa fa-1p3x fa-tags"></i>
                            &nbsp;&nbsp;Tags
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="<?= BASE_URL ?>admin/cadastro-tag">
                                    <i class="fa fa-plus"></i> &nbsp;&nbsp;&nbsp;
                                    Adicionar tag
                                </a>
                            </li>
                            <li>
                                <a href="<?= BASE_URL ?>admin/lista-tags">
                                    <i class="fa fa-reorder"></i> &nbsp;&nbsp;&nbsp;
                                    Listar tags
                                </a>
                            </li>
                        </ul>
                    </li>
                    <!-- INSTITUCIONAL -->
                    <li class="hidden-sm hidden-md<?= strpos($uri, 'institucionais') ? ' active' : ''; ?>">
                        <a href="<?= BASE_URL ?>admin/atualiza-institucional/1">
                            <i class="fa fa-institution"></i>
                            &nbsp;&nbsp;Institucional
                        </a>
                    </li>

                    <!-- Small Small Small  Small  Small  Small  Small  Small  Small  Small  Small  Small  Small  Small  -->
                    <li class="hidden-xs hidden-md hidden-lg<?= (strpos($uri, 'cliente') || strpos($uri, 'portfolio') || strpos($uri, 'tag') || strpos($uri, 'institucionais')) ? ' active' : ''; ?>">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">
                            &nbsp;&nbsp;Outros
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- OPÇÕES PARA CLIENTES -->
                            <li class="dropdown-header">Clientes</li>
                            <li>
                                <a href="<?= BASE_URL ?>admin/cadastro-cliente">
                                    <i class="fa fa-plus"></i> &nbsp;&nbsp;&nbsp;
                                    Adicionar cliente
                                </a>
                            </li>
                            <li>
                                <a href="<?= BASE_URL ?>admin/lista-cliente">
                                    <i class="fa fa-reorder"></i> &nbsp;&nbsp;&nbsp;
                                    Listar cliente
                                </a>
                            </li>
                            <!-- OPÇÕES PARA PORTIFÓLIO -->
                            <li class="dropdown-header">Portifolio</li>
                            <li>
                                <a href="<?= BASE_URL ?>admin/cadastro-portifolio">
                                    <i class="fa fa-plus"></i> &nbsp;&nbsp;&nbsp;
                                    Adicionar portifolio
                                </a>
                            </li>
                            <li>
                                <a href="<?= BASE_URL ?>admin/lista-portifolio">
                                    <i class="fa fa-reorder"></i> &nbsp;&nbsp;&nbsp;
                                    Listar portifolio
                                </a>
                            </li>
                            <!-- OPÇÕES PARA TAGS -->
                            <li class="dropdown-header">Tags</li>
                            <li>
                                <a href="<?= BASE_URL ?>admin/cadastro-tag">
                                    <i class="fa fa-plus"></i> &nbsp;&nbsp;&nbsp;
                                    Adicionar tag
                                </a>
                            </li>
                            <li>
                                <a href="<?= BASE_URL ?>admin/lista-tag">
                                    <i class="fa fa-reorder"></i> &nbsp;&nbsp;&nbsp;
                                    Listar tags
                                </a>
                            </li>
                            <!-- INSTITUCIONAL -->
                            <li class="dropdown-header">Institucional</li>
                            <li>
                                <a href="<?= BASE_URL ?>admin/lista-institucionais">
                                    <i class="fa fa-institution"></i>
                                    &nbsp;&nbsp;Institucional
                                </a>
                            </li>
                        </ul>
                    </li>

                    <!-- Medium Medium Medium Medium Medium Medium Medium Medium Medium Medium Medium Medium Medium Medium -->
                    <li class="hidden-xs hidden-sm hidden-lg<?= (strpos($uri, 'tag') || strpos($uri, 'institucionais')) ? ' active' : ''; ?>">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">
                            &nbsp;&nbsp;Outros
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- OPÇÕES PARA TAGS -->
                            <li class="dropdown-header">Tags</li>
                            <li>
                                <a href="<?= BASE_URL ?>admin/cadastro-tag">
                                    <i class="fa fa-plus"></i> &nbsp;&nbsp;&nbsp;
                                    Adicionar tag
                                </a>
                            </li>
                            <li>
                                <a href="<?= BASE_URL ?>admin/lista-tag">
                                    <i class="fa fa-reorder"></i> &nbsp;&nbsp;&nbsp;
                                    Listar tags
                                </a>
                            </li>
                            <!-- INSTITUCIONAL -->
                            <li class="dropdown-header">Institucional</li>
                            <li>
                                <a href="<?= BASE_URL ?>admin/lista-institucionais">
                                    <i class="fa fa-institution"></i>
                                    &nbsp;&nbsp;Institucional
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a style="color:red; background: #fff; border-radius: 5px;" class="text-danger"
                           href="<?= BASE_URL ?>admin/inicial/sair">
                            <i class="fa fa-sign-out"></i>
                            <span class="">Sair</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- Todo o conteúdo da página -->
    <div class="container">
        <?php include('content/' . $content_file); ?>
    </div>
    <br/>
    <p class="text-center">© 2017 Cieb Engenharia - Todos os direitos reservados</p>
    <!-- Jquery -->
    <script src="<?= BASE_URL ?>/public/libs/bootstrap/js/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?= BASE_URL ?>/public/libs/bootstrap/js/bootstrap.min.js"></script>
    <!-- Data tables -->
    <script src="<?= BASE_URL ?>/public/libs/data-tables/js/jquery.dataTables.js"></script>
    <script src="<?= BASE_URL ?>/public/libs/data-tables/js/dataTables.bootstrap.js"></script>

    <script src="<?= BASE_URL ?>/public/libs/ckeditor/ckeditor.js"></script>
    <script>
        $(document).ready(function () {

            var $$table = $('.datatable').DataTable({
                "dom": "<t>p"
            });

            $("#datatablessearch").on('keyup', function () {
                $$table.search(this.value).draw();
            });

            var $$editor = $('.ck-editor').each(function (key, el) {
                CKEDITOR.replace(el, {
                    toolbar: 'Basic',
                    uiColor: '#ffffff'
                });
            });

            var form = $('.formAttFoto');

            form.on('submit', function (e) {
                var _self = this;
                $.ajax({
                    type: 'POST',
                    url: this.getAttribute('action'),
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (resposta) {
                        if (resposta) {
                            alert('Foto atualizada com sucesso');
                        } else {
                            alert('Erro ao atualizar foto');
                        }

                        window.setTimeout(function () {
                            $(".alert").fadeTo(500, 0).slideUp(500, function () {
                                $(this).remove();
                            });
                        }, 4000);
                    }
                });
                e.preventDefault();
            });
        });    
    </script>

    </body>
    </html>
<?php
ob_flush();
?>