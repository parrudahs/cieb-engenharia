<?php

    $id = !empty($_REQUEST['id']) ? (int) $_REQUEST['id'] : null; 
    
    $produto = new Produto($id);	
    ProdutosDAO::setObject($produto);
    ProdutosDAO::DBConnection();
    ProdutosDAO::getObjectDBData();

?>

<h1>Formul�rio para inserir \ remover Imagens ao Produto</h1>
<p class="cancel">
    <a href="lista-produtos.php"><img src="images/voltar.png" title="cancelar" alt="cancelar" border="0" /></a>
</p>
<?php

    Forms::setFormName("frm-add-pics");
    $outputMessage=Forms::getOutputMessage();
    if ($outputMessage) {
        echo $outputMessage;
        Forms::resetOutputMessage();
    }
    
    Forms::setFormName("frm-del-pics");
    $outputMessage=Forms::getOutputMessage();
    if ($outputMessage) {
        echo $outputMessage;
        Forms::resetOutputMessage();
    } 

?>
<h2>Dados do Produto</h2>
<ul class="desc">
    <li><span>C�digo:</span>&nbsp;<?= ($produto->getCodigo() ? $produto->getCodigo() : "-")  ?></li>
    <li><span>Nome:</span>&nbsp;<?= $produto->getNome() ?></li>
    <li><span>Fornecedor:</span>&nbsp;<?= ListasUtil::getFornecedor($produto->getFornecedorID()) ?></li>
</ul>
<h3 style="padding-left: 15px;">Adicionar fotos</h3>                
<div id="content-fotos" class="tb-form" style="width: 100%;">    
    <form id="frm-add-pics" name="frm-add-pics" method="POST" enctype="multipart/form-data" action="<?= DIR_SYS."/core/Controllers/Controllers.php?face=backend&object=produto&action=adicionar-fotos&id=".$produto->getProdutoID() ?>">
        <table id="tb-add-fotos">
            <tbody id="tbody-fotos">
                <tr>
                    <td>
                        <input type="file" name="foto-file[]" />
                        <input type="image" title="Remover" src="images/remove_small.png" onclick="removeFieldFoto(this);" />
                    </td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td style="text-align: right;">
                        <img src="images/add_medium.png" title="Mais fotos" style="cursor: pointer;" onclick="addFieldFoto();" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="image" src="images/salvar.png" onclick="javascript:submitForm('frm-add-pics');" style="cursor: pointer" title="Cadastrar fotos" />
                    </td>
                </tr>                           
            </tfoot>
        </table>
    </form>
</div>
<hr style="border: none; border-bottom: 1px solid #CCC;" />
<h3 style="padding-left: 15px;">Fotos cadastradas</h3>
<form id="frm-del-pics" name="frm-del-pics" method="POST" action="<?= DIR_SYS."/core/Controllers/Controllers.php?face=backend&object=produto&action=deletar-fotos&id=".$produto->getProdutoID() ?>">
    <table id="tb-galeria" class="tb-form">
        <tbody>
            <?php

                $fotos = $produto->getFotos();

                $colls=0;
                if (sizeof($fotos) > 0) {
                    foreach ($fotos as $f) {
                        $src = "../../imagens/produtos/produto".$produto->getProdutoID()."/thumb-".$f;
                        if ($colls%6==0) { print("<tr>"); }
                        print("<td><img src=\"".$src."\" /><p><input type=\"checkbox\" name=\"foto[]\" value=\"".$f."\" />&nbsp;Excluir</p>");
                        $colls++;
                    }
                }
                
            ?>
            <tr>
                <td colspan="4" style="padding-top: 10px;">
                    <input type="image" src="images/remover.png" title="Excluir imagens selecionadas" onclick="javascript:submitForm('frm-del-pics');" />
                </td>
            </tr>
        </tbody>
    </table>
</form>