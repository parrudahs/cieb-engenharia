<?php
    $att = new Jcms\Core\Controllers\ClienteController();
    $id = $urls[2];
    $att->show($id);
?>

<div class="row">
   <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-center">
        <img src="<?= BASE_URL ?>/images/logo.png" title="CIEB engenharia"
             style="margin-top: 40px;margin-bottom: 50px"/>
        <ul class="list-group">
            <a class="list-group-item" href="<?= BASE_URL ?>admin/inicial">
                Página inicial
            </a>
            <li class="list-group-item">
                Ver todos os clientes
            </li>
            <a class="list-group-item" href="<?= BASE_URL ?>admin/cadastro-cliente">
                Cadastrar cliente
            </a>
        </ul>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="text-uppercase text-info">
                    Atualização
                    <small>Formulário para atualizar cliente</small>
                </h3>
            </div>
            <div class="panel-body">
                <form method="post" action="<?= BASE_URL ?>action/clientes/atualizar/<?= $id ?>"
                      enctype="multipart/form-data">
                    <?php if (isset($_SESSION['output_message'])): ?>
                        <div class='alert alert-<?= $_SESSION['output_message_tipo'] ?>'>
                            <strong class='msgError'><?= $_SESSION['output_message'] ?></strong>
                        </div>
                        <?php unset($_SESSION['output_message']); ?>
                    <?php endif; ?>
                    <!-- Nome do cliente -->
                    <div class="input-group">
                        <div class="input-group-addon">Nome:</div>
                        <input class="form-control" id="nome" name="nome"
                               value="<?= isset($_SESSION['formulario_cliente']['nome']) ? $_SESSION['formulario_cliente']['nome'] : null ?>" required/>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                            <!-- Site do cliente -->
                            <div class="input-group">
                                <div class="input-group-addon">Site:</div>
                                <input class="form-control" id="site" name="site"
                                       value="<?= isset($_SESSION['formulario_cliente']['site']) ? $_SESSION['formulario_cliente']['site'] : null ?>"/>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-center">
                            <!-- Imagem cliente -->
                            <label for="upload" style="cursor: pointer">
                                <i class="fa fa-2x fa-folder-open-o"></i>
                                <input id="upload" type="file" name="imagem" accept="image/*" style="display:none"/>
                                Selecionar imagem
                            </label>
                        </div>
                    </div>
                    <!-- Descrição de notícia -->
                    <div class="input-group with-ck-editor">
                        <div class="input-group-addon">Descrição:&nbsp;&nbsp;&nbsp;</div>
                        <textarea class="form-control ck-editor" id="descricao" name="descricao">
                            <?= isset($_SESSION['formulario_cliente']['descricao']) ? $_SESSION['formulario_cliente']['descricao'] : null ?>
                        </textarea>
                    </div>
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-1 col-lg-6">

                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-1 col-lg-6 text-right">
                            <input type="hidden" value="Atualizar" name="atualizar" />
                            <button class="btn btn-primary" type="submit">
                                <i class="fa fa-2x fa-edit"></i>
                                Atualizar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>