<?php
$port = new Jcms\Core\Controllers\PortfolioController;
?>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-center">
        <img src="<?= BASE_URL ?>/images/logo.png" title="CIEB engenharia"
             style="margin-top: 40px;margin-bottom: 50px"/>
        <ul class="list-group">
            <a class="list-group-item" href="<?= BASE_URL ?>admin/inicial">
                Página inicial
            </a>
            <li class="list-group-item">
                Ver todos os portifólios
            </li>
            <a class="list-group-item" href="<?= BASE_URL ?>admin/cadastro-portfolio">
                Cadastrar potifólio
            </a>
        </ul>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="text-uppercase text-info">
                    Portifólio
                    <small>Todos os portifólios</small>
                </h3>
            </div>
            <div class="panel-body">
                <?php if (isset($_SESSION['output_message'])) { ?>
                    <div class='alert alert-<?= $_SESSION['output_message_tipo'] ?> alert-dismissable'>
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <strong class='msgError'><?= $_SESSION['output_message'] ?></strong>
                    </div>
                    <?php unset($_SESSION['output_message']);
                } ?>
                <form>
                    <div class="input-group">
                        <input id="datatablessearch" class="form-control" type="text"
                               placeholder="Pesquisar por portifólio"/>
                        <div class="input-group-addon">
                            <i class="fa fa-search"></i>
                        </div>
                    </div>
                </form>
                <table class="datatable table table-striped">
                    <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Tag</th>
                        <th>Foi publicado?</th>
                        <th>Ações</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($port->listaTodosTrabalhos() as $dest) {
                        ?>
                        <tr>
                            <td><?= $dest['nome'] ?></td>
                            <td><?= $dest['tag_id'] ?></td>
                            <td><?= ($dest['publicado']) ? "<i class='fa fa-check text-success'></i>" : "<i class='fa fa-close text-danger'></i>" ?></td>
                            <td>
                                <a href="<?= BASE_URL ?>admin/gerencia-fotos/<?= $dest['id'] ?>" title="Gerenciar Fotos.">
                                    <i class="fa fa-cog"></i>
                                </a>
                                &nbsp;&nbsp;&nbsp;
                                <a href="<?= BASE_URL ?>admin/atualiza-portfolio/<?= $dest['id'] ?>" title="Editar trabalho!">
                                    <i class="fa fa-edit"></i>
                                </a>
                                &nbsp;&nbsp;&nbsp;
                                <a href="#"
                                   onclick="javascript: if (confirm('Você realmente deseja excluir o Trabalho?')) location.href = '<?= BASE_URL ?>action/portfolio/deletar/<?= $dest['id'] ?>'">
                                    <i class="fa fa-trash-o text-danger" title="Deletar trabalho!"></i>
                                </a>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>