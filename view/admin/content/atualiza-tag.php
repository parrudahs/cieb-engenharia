<?php
    $att = new Jcms\Core\Controllers\TagController();
    $id = $urls[2];
    $att->show($id);
?>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4"></div>
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="text-uppercase text-info">
                    Tag
                    <small>Formulário para atualizar tag</small>
                </h3>
            </div>
            <div class="panel-body">
                <form method="POST" enctype="multipart/form-data" action="<?= BASE_URL ?>action/tags/atualizar/<?= $id ?>">
                    <?php if (isset($_SESSION['output_message'])) { ?>
                        <div class='alert alert-<?= $_SESSION['output_message_tipo'] ?>'>
                            <strong class='msgError'><?= $_SESSION['output_message'] ?></strong>
                        </div>
                        <?php unset($_SESSION['output_message']);
                    } ?>

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="input-group">
                                <div class="input-group-addon">Nome:</div>
                                <input class="form-control" id="nome" name="nome"
                                       value="<?= isset($_SESSION['formulario_tag']['nome']) ? $_SESSION['formulario_tag']['nome'] : null ?>" required/>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" value="Atualizar" name="atualizar"/>
                    <button class="btn btn-primary" type="submit">
                        <i class="fa fa-edit"></i>
                        Atualizar
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>