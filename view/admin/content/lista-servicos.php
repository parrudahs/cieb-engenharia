<?php
$servicos = new \Jcms\Core\Controllers\ServicoController();
?>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-center">
            <img src="<?= BASE_URL ?>/images/logo.png" title="CIEB engenharia"
                 style="margin-top: 40px;margin-bottom: 50px"/>
            <ul class="list-group">
                <a class="list-group-item" href="<?= BASE_URL ?>admin/inicial">
                    Página inicial
                </a>
                <li class="list-group-item">
                    Ver todos os serviços
                </li>
                <a class="list-group-item" href="<?= BASE_URL ?>admin/cadastro-servico">
                    Cadastrar serviço
                </a>
            </ul>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="text-uppercase text-info">
                        Serviço
                        <small>Todos os serviços</small>
                    </h3>
                </div>
                <div class="panel-body">
                    <?php if (isset($_SESSION['output_message'])) { ?>
                        <div class='alert alert-<?= $_SESSION['output_message_tipo'] ?> alert-dismissable'>
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong class='msgError'><?= $_SESSION['output_message'] ?></strong>
                        </div>
                        <?php unset($_SESSION['output_message']);
                    } ?>
                    <form>
                        <div class="input-group">
                            <input id="datatablessearch" class="form-control" type="text"
                                   placeholder="Pesquisar por destaque"/>
                            <div class="input-group-addon">
                                <i class="fa fa-search"></i>
                            </div>
                        </div>
                    </form>
                    <table class="datatable table table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Data</th>
                            <th>Nome</th>
                            <th>Foi publicado?</th>
                            <th>Ações</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $s = $servicos->listaTodosServicosPainel();
                        if ($servicos->getRowCount() > 0) {
                            foreach ($s as $servico) {
                                ?>
                                <tr>
                                    <td><?= $servico['id'] ?></td>
                                    <td><?= date('d/m/Y', strtotime($servico['data'])); ?></td>
                                    <td><?= $servico['titulo'] ?></td>
                                    <td><?= ($servico['publicado']) ? "<i class='fa fa-check text-success'></i>" : "<i class='fa fa-2x fa-close text-danger'></i>" ?></td>
                                    <td>
                                        &nbsp;&nbsp;&nbsp;
                                        <a href="<?= BASE_URL ?>admin/atualiza-servico/<?= $servico['id'] ?>">
                                            <i class="fa fa-edit" title="Editar Serviço."></i>
                                        </a>
                                        &nbsp;&nbsp;&nbsp;
                                        <a href="#"
                                           onclick="javascript: if (confirm('Você realmente deseja excluir o servico?')) location.href='<?= BASE_URL ?>action/servicos/deletar/<?= $servico['id'] ?>'">
                                            <i class="fa fa-trash-o text-danger" title="Deletar Serviço."></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php }
                        } else
                            print("<tr><td colspan='7'>- nenhum registro cadastrado -</td></tr>");
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php Jcms\Core\Ext\Forms::unsetFormData('servico'); ?>