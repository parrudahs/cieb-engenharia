<?php
$config = new \Jcms\Core\Controllers\ConfiguracaoController();
if (isset($_POST['atualizar'])) {
    $config->updateConfiguracao($_POST);
}

?>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="alert alert-success">
            <i class="fa fa-2x fa-user-o"></i>
            &nbsp;Olá <strong><?= $_SESSION['instances']['backend']['user'] ?></strong>, Seja bem-vindo(a) ao Sistema de
            Gerenciamento de Conteúdo do <i>web site</i> <strong>Cieb Engenharia</strong>
        </div>
    </div>
</div>

<?php
if (isset($_SESSION['output_message'])) {
    print "<div class='alert alert-" . $_SESSION['output_message_tipo'] . "'>";
    print("<p class='msgError'>" . $_SESSION['output_message'] . "</p>");
    unset($_SESSION['output_message']);
    print "</div>";
}
?>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 text-center">
        <img src="<?= BASE_URL ?>/images/logo.png" title="CIEB engenharia"/>
        <br/>
        <br/>
        <p>Página de acesso</p>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="text-center">
                    <i class="fa fa-address-book-o"></i>
                    Configurações de acesso
                </h4>
            </div>
            <div class="panel-body">
                <form name="frm-configs" method="POST" action="<?= BASE_URL ?>admin/inicial">
                    <?php $configs = $config->indexPainel(); ?>
                    <label for="senha">Alterar a senha do sistema:</label>
                    <div class="input-group">
                        <div class="input-group-addon">Nova senha:</div>
                        <input class="form-control" type="password" name="senha"/>
                    </div>
                    <div class="input-group">
                        <div class="input-group-addon">Confirmar senha:</div>
                        <input class="form-control" type="password" name="re-senha"/>
                    </div>

                    <label for="senha">Alterar o Email do sistema:</label>
                    <div class="input-group">
                        <div class="input-group-addon"></div>
                        <textarea class="form-control" name="emails"><?= $configs['EMAILS_CONTATO'] ?></textarea>
                    </div>
                    <input class="btn btn-primary" type="submit" value="Atualizar configurações" name="atualizar"/>
                </form>
            </div>
        </div>
    </div>
</div>