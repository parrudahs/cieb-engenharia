<?php
$att = new Jcms\Core\Controllers\DestaqueController();
$id = $urls[2];
$att->show($id);
?>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-center">
        <img src="<?= BASE_URL ?>/images/logo.png" title="CIEB engenharia"
             style="margin-top: 40px;margin-bottom: 50px"/>
        <ul class="list-group">
            <a class="list-group-item" href="<?= BASE_URL ?>admin/inicial">
                Página inicial
            </a>
            <li class="list-group-item">
                Ver todos os destaques
            </li>
            <a class="list-group-item" href="<?= BASE_URL ?>admin/cadastro-destaque">
                Cadastrar destaque
            </a>
        </ul>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="text-uppercase text-info">
                    Atualização
                    <small>Formulário para atualização de destaque</small>
                </h3>
            </div>
            <div class="panel-body">
                <form method="POST" enctype="multipart/form-data"
                      action="<?= BASE_URL ?>action/destaques/atualizar/<?= $id ?>">
                    <?php if (isset($_SESSION['output_message'])) { ?>
                        <div class='alert alert-<?= $_SESSION['output_message_tipo'] ?>'>
                            <strong class='msgError'><?= $_SESSION['output_message'] ?></strong>
                        </div>
                        <?php unset($_SESSION['output_message']);
                    } ?>

                    <div class="row">
                        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                            <!-- Título do destaque -->
                            <div class="input-group">
                                <div class="input-group-addon">Título:</div>
                                <input class="form-control" id="titulo" name="titulo"
                                       value="<?= isset($_SESSION['formulario_destaque']['titulo']) ? $_SESSION['formulario_destaque']['titulo'] : null ?>"
                                       required/>

                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-center">
                            <!-- Imagem do destaque -->
                            <label for="upload" style="cursor: pointer">
                                <i class="fa fa-2x fa-folder-open-o"></i>
                                <input id="upload" type="file" name="imagem" style="display:none"/>
                                Selecionar imagem
                            </label>
                        </div>
                    </div>
                    <!-- Resumo do destaque -->
                    <div class="input-group with-ck-editor">
                        <div class="input-group-addon">Resumo:&nbsp;&nbsp;&nbsp;</div>
                        <textarea class="form-control ck-editor" name="resumo">
                                <?= isset($_SESSION['formulario_destaque']['resumo']) ? $_SESSION['formulario_destaque']['resumo'] : null ?>
                            </textarea>
                    </div>
                    <!-- Conteúdo do destaque -->
                    <div class="input-group with-ck-editor">
                        <div class="input-group-addon">Conteúdo:&nbsp;&nbsp;&nbsp;</div>
                        <textarea class="form-control ck-editor" name="corpo">
                                <?= isset($_SESSION['formulario_destaque']['corpo']) ? $_SESSION['formulario_destaque']['corpo'] : null ?>
                            </textarea>
                    </div>

                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-1 col-lg-6">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox"
                                           name="publicado" <?= isset($_SESSION['formulario_destaque']['publicado']) && $_SESSION['formulario_destaque']['publicado'] == '1' ? "checked" : null ?>>
                                    Marque para publicar, ou desmarque para não publicar.
                                </label>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-1 col-lg-6 text-right">
                            <input class="btn btn-success" type="submit" value="Atualizar" name="cadastrar"/>
                        </div>
                    </div>
                </form>
                <?php Jcms\Core\Ext\Forms::unsetFormData('destaque'); ?>
            </div>
        </div>
    </div>
</div>