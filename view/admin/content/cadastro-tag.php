<?php
$cad = new Jcms\Core\Controllers\TagController();
?>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 text-center">
        <img src="<?= BASE_URL ?>/images/logo.png" title="CIEB engenharia"
             style="margin-top: 0;margin-bottom: 50px"/>
        <ul class="list-group">
            <a class="list-group-item" href="<?= BASE_URL ?>admin/inicial">
                Página inicial
            </a>
            <a class="list-group-item" href="<?= BASE_URL ?>admin/lista-tags">
                Ver todos as tags
            </a>
            <li class="list-group-item">Cadastro de tag</li>
        </ul>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="text-uppercase text-info">
                    Tag
                    <small>Formulário de cadastro de tag</small>
                </h3>
            </div>
            <div class="panel-body">
                <form method="POST" enctype="multipart/form-data" action="<?= BASE_URL ?>action/tags/cadastrar">
                    <?php if (isset($_SESSION['output_message'])) { ?>
                        <div class='alert alert-<?= $_SESSION['output_message_tipo'] ?>'>
                            <strong class='msgError'><?= $_SESSION['output_message'] ?></strong>
                        </div>
                        <?php unset($_SESSION['output_message']);
                    } ?>

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="input-group">
                                <div class="input-group-addon">Nome:</div>
                                <input class="form-control" id="nome" name="nome"
                                       value="<?= isset($_SESSION['formulario_tag']['nome']) ? $_SESSION['formulario_tag']['nome'] : null ?>" required/>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" value="Cadastrar" name="cadastrar"/>
                    <button class="btn btn-success" type="submit">
                        <i class="fa fa-plus-circle"></i>
                        Adicionar
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
