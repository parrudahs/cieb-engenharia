<?php
$trabalho = new Jcms\Core\Controllers\PortfolioController();

$foto = new Jcms\Core\Controllers\FotoController();

$trab = $trabalho->show($urls[2]);

unset($_SESSION['formulario_portfolio']);

?>

<div class="panel panel-success">
    <div class="panel-heading">
        <h2><?= $trab['nome'] ?></h2>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right">
                <a href="<?= BASE_URL ?>admin/atualiza-portfolio/<?= $trab['id'] ?>">
                    <i class="fa fa-1p3x fa-edit"></i> Editar galeria
                </a>
                &nbsp;&nbsp;&nbsp;
                <a class="text-danger" href="#">
                    <i class="fa fa-1p3x fa-trash-o"></i> Remover galeria
                </a>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <?php if (isset($_SESSION['output_message'])) { ?>
            <div class='alert alert-<?= $_SESSION['output_message_tipo'] ?> alert-dismissable'>
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong class='msgError'><?= $_SESSION['output_message'] ?></strong>
            </div>
        <?php unset($_SESSION['output_message']); } ?>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h4>Adicionar Fotos:</h4>
                <form action="<?= BASE_URL ?>action/portfolio/adicionar-fotos/<?= $trab['id'] ?>" class="dropzone" id="my-awesome-dropzone">
                </form>
            </div>
        </div>
        <br/>
        <br/>
        <br/>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h4>Fotos do trabalho:</h4>

                <?php 
                $fotos = $foto->getFotosGaleria($urls[2]);
                if ($foto->getRowCount() > 0) {
                    $i = 0;
                    foreach ($fotos as $f) { 
                ?>
                    <div class="col-xs-4 col-md-3">
                        <div class="thumbnail">
                            <img src="<?= BASE_URL ?>public/uploaded_files/portfolio/350x-<?= $f['imagem'] ?>" alt="" class="img-responsive">
                            <div class="caption">
                                <div class="row">
                                    <form id="form<?= $i ?>" action="<?= BASE_URL ?>action/portfolio/atualizar-fotos/<?= $urls[2] ?>/<?= $f['id'] ?>" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 formAttFoto" method="POST">
                                        <input type="hidden" class="form-control" name="imagem" value="<?= $f['imagem'] ?>"/>
                                        <input type="hidden" class="form-control" name="portfolio_id" value="<?= $f['portfolio_id'] ?>"/>
                                        <div class="input-group-addon">Descrição:</div>
                                        <textarea class="form-control" id="descricao" name="descricao"><?= $f['descricao'] ?></textarea>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <label for="publicado">Publicada:</label>
                                                <br/>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="checkbox" name="publicada" <?= isset($f['publicar']) && $f['publicar'] == '1' ? "checked" : null ?> /> 
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <label for="capa">Capa:</label>
                                                <br/>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="checkbox" name="capa" <?= isset($f['capa']) && $f['capa'] == '1' ? "checked" : null ?> /> 
                                            </div>
                                        </div>

                                        <input class="btn btn-success" type="submit" value="Atualizar Foto" name="atualizar" />   

                                        <a href="#" class="btn btn-danger" onclick="javascript: if (confirm('Você realmente deseja excluir o destaque?'))
                                            location.href = '<?= BASE_URL ?>action/portfolio/deletar-foto/<?= $f['id'] ?>/<?= $f['portfolio_id'] ?>'">
                                            <i class="fa fa-trash-o text-danger" title="Deletar destaque."></i>
                                        </a>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                <?php 
                    }
                } else {
                ?> 
                    <h3>Esse trabalho não tem nenhuma foto.</h3> 
                <?php
                }
                ?>
            </div>

        </div>
    </div>
</div>
</div>

<script src="<?= BASE_URL ?>/public/libs/dropzone/dropzone.js"></script>
<script>
  Dropzone.options.myAwesomeDropzone = {
    addRemoveLinks: false,
    dictDefaultMessage: "Arraste e solte a foto aqui!",
    init: function() {
      this.on("queuecomplete", function(file) {  
        location.reload();
      });
    }
  };
</script>


