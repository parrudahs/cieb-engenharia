<?php
$cad = new Jcms\Core\Controllers\NoticiaController();
?>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-center">
        <img src="<?= BASE_URL ?>/images/logo.png" title="CIEB engenharia"
             style="margin-top: 40px;margin-bottom: 50px"/>
        <ul class="list-group">
            <a class="list-group-item" href="<?= BASE_URL ?>admin/inicial">
                Página inicial
            </a>
            <a class="list-group-item" href="<?= BASE_URL ?>admin/lista-clientes">
                Ver todos os clientes
            </a>
            <li class="list-group-item">Cadastro de cliente</li>
        </ul>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="text-uppercase text-info">
                    Cliente
                    <small>Formulário de cadastro de cliente</small>
                </h3>
            </div>
            <div class="panel-body">
                <form method="post" action="<?= BASE_URL ?>action/clientes/cadastrar"
                      enctype="multipart/form-data">
                    <?php if (isset($_SESSION['output_message'])): ?>
                        <div class='alert alert-<?= $_SESSION['output_message_tipo'] ?>'>
                            <strong class='msgError'><?= $_SESSION['output_message'] ?></strong>
                        </div>
                        <?php unset($_SESSION['output_message']); ?>
                    <?php endif; ?>
                    <!-- Nome do cliente -->
                    <div class="input-group">
                        <div class="input-group-addon">Nome:</div>
                        <input class="form-control" id="nome" name="nome"
                               value="<?= isset($_SESSION['formulario_cliente']['nome']) ? $_SESSION['formulario_cliente']['nome'] : null ?>" required/>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                            <!-- Site do cliente -->
                            <div class="input-group">
                                <div class="input-group-addon">Site:</div>
                                <input class="form-control" id="site" name="site"
                                       value="<?= isset($_SESSION['formulario_cliente']['site']) ? $_SESSION['formulario_cliente']['site'] : null ?>"/>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-center">
                            <!-- Imagem cliente -->
                            <label for="upload" style="cursor: pointer">
                                <i class="fa fa-2x fa-folder-open-o"></i>
                                <input id="upload" type="file" name="imagem" accept="image/*" style="display:none" />
                                Selecionar imagem
                            </label>
                        </div>
                    </div>
                    <!-- Descrição de notícia -->
                    <div class="input-group with-ck-editor">
                        <div class="input-group-addon">Descrição:&nbsp;&nbsp;&nbsp;</div>
                        <textarea class="form-control ck-editor" id="descricao" name="descricao">
                            <?= isset($_SESSION['formulario_cliente']['descricao']) ? $_SESSION['formulario_cliente']['descricao'] : null ?>
                        </textarea>
                    </div>
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-1 col-lg-6">

                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-1 col-lg-6 text-right">
                            <input type="hidden" value="Cadastrar" name="cadastrar"/>
                            <button class="btn btn-success" type="submit">
                                <i class="fa fa-2x fa-plus-circle"></i>
                                Adicionar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>