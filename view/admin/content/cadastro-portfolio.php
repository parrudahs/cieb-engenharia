<?php
$portfolio = new Jcms\Core\Controllers\PortfolioController();
?>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-center">
        <img src="<?= BASE_URL ?>/images/logo.png" title="CIEB engenharia"
             style="margin-top: 40px;margin-bottom: 50px"/>
        <ul class="list-group">
            <a class="list-group-item" href="<?= BASE_URL ?>admin/inicial">
                Página inicial
            </a>
            <a class="list-group-item" href="<?= BASE_URL ?>admin/lista-portfolio">
                Ver todos os portifólios
            </a>
            <li class="list-group-item">Cadastro de portifólio</li>
        </ul>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="text-uppercase text-info">
                    Portifólio
                    <small>Cadastro de Trabalho</small>
                </h3>
            </div>
            <div class="panel-body">
                <form method="post" enctype="multipart/form-data" action="<?= BASE_URL ?>action/portfolio/cadastrar">
                    <?php if (isset($_SESSION['output_message'])) { ?>
                        <div class='alert alert-<?= $_SESSION['output_message_tipo'] ?>'>
                            <strong class='msgError'><?= $_SESSION['output_message'] ?></strong>
                        </div>
                        <?php unset($_SESSION['output_message']);
                    } ?>

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                            <div class="input-group">
                                <div class="input-group-addon">Nome:</div>
                                <input class="form-control" id="nome" name="nome"
                                       value="<?= isset($_SESSION['formulario_portfolio']['nome']) ? $_SESSION['formulario_portfolio']['nome'] : null ?>"
                                       required/>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                            <div class="input-group">
                                <div class="input-group-addon">Tag</div>
                                <select class="form-control" name="tag_id">
                                    <option value="0">- nenhum -</option>
                                    <?php foreach ($portfolio->getTags() as $tag) { ?>
                                        <option value="<?= $tag['id'] ?>"><?= $tag['nome'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>


                    <div class="input-group">
                        <div class="input-group-addon">Local:</div>
                        <input class="form-control" id="local" name="local"
                               value="<?= isset($_SESSION['formulario_portfolio']['local']) ? $_SESSION['formulario_portfolio']['local'] : null ?>" required/>
                    </div>


                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                            <div class="input-group">
                                <div class="input-group-addon">Estado:</div>
                                <input class="form-control" id="estado" name="estado"
                                       value="<?= isset($_SESSION['formulario_portfolio']['estado']) ? $_SESSION['formulario_portfolio']['estado'] : null ?>"
                                       required/>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                            <div class="input-group">
                                <div class="input-group-addon">Sigla:</div>
                                <input class="form-control" id="sigla" name="sigla"
                                       value="<?= isset($_SESSION['formulario_portfolio']['sigla']) ? $_SESSION['formulario_portfolio']['sigla'] : null ?>"
                                       required/>
                            </div>
                        </div>
                    </div>
                    
                    <div class="input-group with-ck-editor">
                        <div class="input-group-addon">Descrição:&nbsp;&nbsp;&nbsp;</div>
                        <textarea class="form-control ck-editor" name="descricao">
                                <?= isset($_SESSION['formulario_portfolio']['descricao']) ? $_SESSION['formulario_portfolio']['descricao'] : null ?>
                            </textarea>
                    </div>
                            

                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-1 col-lg-6">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="publicado"> Marque se deseja publicar agora !
                                </label>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-1 col-lg-6 text-right">
                            <input type="hidden" value="Cadastrar" name="cadastrar"/>
                            <button class="btn btn-success" type="submit">
                                <i class="fa fa-2x fa-plus-circle"></i>
                                Cadastrar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>