<?php 
$intituicao = new \Jcms\Core\Controllers\InstitucionalController();
$id = $urls[2];
$att = $intituicao->show($id);    
?>
<div class="page-header">
    <h3 class="text-uppercase text-info">
        Formulário para atualizar Conteúdo Institucional
        <small><a class="btn btn-default" target="_blank" href="<?= BASE_URL ?>/institucional">Visualizar Página</a></small>
    </h3>
</div>
<div class="row">
    <div class="panel panel-default col-xs-12 col-sm-12 col-md-10 col-lg-10">
        <form id="frm-add-destaque" name="frm-add-destaque" method="POST" enctype="multipart/form-data" action="<?= BASE_URL ?>action/institucionais/atualizar/<?= $att['id'] ?>">
           
           <?php if (isset($_SESSION['output_message'])) { ?>
                <div class='alert alert-<?= $_SESSION['output_message_tipo'] ?>'>
                    <strong class='msgError'><?= $_SESSION['output_message'] ?></strong>
                </div>
            <?php unset($_SESSION['output_message']); } ?>

            <div class="input-group">
                <div class="input-group-addon">Titulo</div>
                <input class="form-control" id="titulo" name="titulo" value="<?=  $att['titulo'] ?>"/>
                <div class="input-group-addon"><i class="text-danger">Obrigatório</i></div>
            </div>
            <div class="form-group">
                <label for="resumo">Resumo</label>
                <textarea class="form-control ck-editor" id="conteudo" rows="3" name="resumo"><?= $att['resumo'] ?></textarea>
            </div>
            <div class="form-group">
                <label for="resumo">Conteúdo</label>
                <textarea class="form-control ck-editor" id="conteudo" name="corpo"><?= $att['corpo'] ?></textarea>
            </div>
            <div class="form-group">                
                <div class="row">
                    <div class="col-md-9">
                        <div class="input-group">
                           <div class="input-group-addon">Imagem</div>
                            <input class="form-control" id="imagem" type="file" name="banner"/>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="input-group-addon">Imagem Atual</div>
                        <img src="<?= BASE_URL."public/uploaded_files/institucional/".$att['banner']  ?>" class="img-responsive">
                    </div>
                </div>
            </div>

            <input class="btn btn-primary" type="submit" value="Atualizar" name="atualizar" />
        </form>
    </div>
</div>
