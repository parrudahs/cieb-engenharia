<?php
$tag = new \Jcms\Core\Controllers\TagController();
?>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 text-center">
        <img src="<?= BASE_URL ?>/images/logo.png" title="CIEB engenharia"
             style="margin-top: 40px;margin-bottom: 50px"/>
        <ul class="list-group">
            <a class="list-group-item" href="<?= BASE_URL ?>admin/inicial">
                Página inicial
            </a>
            <li class="list-group-item">
                Ver todos as tags
            </li>
            <a class="list-group-item" href="<?= BASE_URL ?>admin/cadastro-tag">
                Cadastrar tag
            </a>
        </ul>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="text-uppercase text-info">
                    Tags
                    <small>Todos as tags</small>
                </h3>
            </div>
            <div class="panel-body">
                <?php if (isset($_SESSION['output_message'])) { ?>
                    <div class='alert alert-<?= $_SESSION['output_message_tipo'] ?> alert-dismissable'>
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <strong class='msgError'><?= $_SESSION['output_message'] ?></strong>
                    </div>
                    <?php unset($_SESSION['output_message']);
                } ?>
                <form>
                    <div class="input-group">
                        <input id="datatablessearch" class="form-control" type="text"
                               placeholder="Pesquisar por destaque"/>
                        <div class="input-group-addon">
                            <i class="fa fa-search"></i>
                        </div>
                    </div>
                </form>
                <table class="datatable table table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Nome</th>
                        <th>Ações</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $s = $tag->listaTodasTags();
                    if ($tag->getRowCount() > 0) {
                        foreach ($s as $t) {
                            ?>
                            <tr>
                                <td><?= $t['id'] ?></td>
                                <td><?= $t['nome'] ?></td>
                                <td>
                                    &nbsp;&nbsp;&nbsp;
                                    <a href="<?= BASE_URL ?>admin/atualiza-tag/<?= $t['id'] ?>">
                                        <i class="fa fa-edit" title="Editar tag!"></i>
                                    </a>
                                    &nbsp;&nbsp;&nbsp;
                                    <a href="#"
                                       onclick="javascript: if (confirm('Você realmente deseja excluir a Tag?')) location.href='<?= BASE_URL ?>action/tags/deletar/<?= $t['id'] ?>'">
                                        <i class="fa fa-trash-o text-danger" title="Deletar tag!"></i>
                                    </a>
                                </td>
                            </tr>
                        <?php }
                    } else
                        print("<tr><td colspan='7'>- nenhum registro cadastrado -</td></tr>");
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>