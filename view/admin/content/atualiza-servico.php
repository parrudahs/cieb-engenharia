<?php
$att = new Jcms\Core\Controllers\ServicoController();
$id = $urls[2];
$att->show($id);
?>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-center">
        <br/>
        <br/>
        <img src="<?= BASE_URL ?>/images/logo.png" title="CIEB engenharia"/>
        <br/>
        <br/>
        <p>
            <a href="<?= BASE_URL ?>admin/inicial">Página inicial</a>
            <br/>
            <i class="fa fa-angle-down"></i>
            <br/>
            <a href="<?= BASE_URL ?>admin/lista-servicos">Ver todos os serviços</a>
            <br/>
            <i class="fa fa-angle-down"></i>
            <br/>
            <?= isset($_SESSION['formulario_servico']['titulo']) ? $_SESSION['formulario_servico']['titulo'] : null ?>
            <br/>
            <br/>
        </p>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="text-uppercase text-info">
                    Cadastro
                    <small>Formulário para cadastrar serviço</small>
                </h3>
            </div>
            <div class="panel-body">
                <form method="post" action="<?= BASE_URL ?>action/servicos/atualizar/<?= $id ?>"
                      enctype="multipart/form-data">
                    <?php if (isset($_SESSION['output_message'])): ?>
                        <div class='alert alert-<?= $_SESSION['output_message_tipo'] ?>'>
                            <strong class='msgError'><?= $_SESSION['output_message'] ?></strong>
                        </div>
                        <?php unset($_SESSION['output_message']); ?>
                    <?php endif; ?>
                    <div class="row">
                        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                            <!-- Título do serviço -->
                            <div class="input-group">
                                <div class="input-group-addon">Título:</div>
                                <input class="form-control" id="titulo" name="titulo"
                                       value="<?= isset($_SESSION['formulario_servico']['titulo']) ? $_SESSION['formulario_servico']['titulo'] : null ?>"
                                       required/>

                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 text-center">
                            <!-- Imagem do serviço -->
                            <label for="upload" style="cursor: pointer">
                                <i class="fa fa-2x fa-folder-open-o"></i>
                                <input id="upload" type="file" name="imagem" style="display:none"/>
                                Selecionar imagem
                            </label>
                        </div>
                    </div>
                    <!-- Resumo do serviço -->
                    <div class="input-group with-ck-editor">
                        <div class="input-group-addon">Resumo:&nbsp;&nbsp;&nbsp;</div>
                        <textarea class="form-control ck-editor" id="conteudo" name="resumo">
                            <?= isset($_SESSION['formulario_servico']['resumo']) ? $_SESSION['formulario_servico']['resumo'] : null ?>
                        </textarea>
                    </div>
                    <!-- Conteúdo do serviço -->
                    <div class="input-group with-ck-editor">
                        <div class="input-group-addon">Conteúdo:&nbsp;&nbsp;&nbsp;</div>
                        <textarea class="form-control ck-editor" name="corpo">
                            <?= isset($_SESSION['formulario_servico']['corpo']) ? $_SESSION['formulario_servico']['corpo'] : null ?>
                        </textarea>
                    </div>
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-1 col-lg-6">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox"
                                           name="publicado" <?= isset($_SESSION['formulario_servico']['publicado']) && $_SESSION['formulario_servico']['publicado'] == '1' ? "checked" : null ?>>
                                    Marque se deseja publicar agora !
                                </label>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-1 col-lg-6 text-right">
                            <input class="btn btn-primary" type="submit" value="Atualizar" name="atualizar"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<?php Jcms\Core\Ext\Forms::unsetFormData('servico'); ?>