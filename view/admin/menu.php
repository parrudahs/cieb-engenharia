<h3 style="background-color: #CCC; margin: 0px; padding: 2px 0px 2px 3px;">Menu</h3>
<ul id="left-menu">
    <li><img src="<?= BASE_URL ?>view/admin/images/desktop.png" /> <a href="<?= BASE_URL ?>admin/inicial"> Inicial</a></li>
    <hr/>
    <li><img src="<?= BASE_URL ?>view/admin/images/institucional.png" /> <a href="<?= BASE_URL ?>admin/lista-institucionais">Institucional</a></li>
    <hr/>
    <li><img src="<?= BASE_URL ?>view/admin/images/destaques.png"  alt="" /> Destaques
        <ul>
            <li>- <a href="<?= BASE_URL ?>admin/cadastro-destaque">Adicionar</a></li>
            <li>- <a href="<?= BASE_URL ?>admin/lista-destaques">Listar</a></li>
        </ul>
    </li>
    <hr/>
    <li><img src="<?= BASE_URL ?>view/admin/images/noticias.png"  alt="" /> Notícias
        <ul>
            <li>- <a href="<?= BASE_URL ?>admin/cadastro-noticia">Adicionar</a></li>
            <li>- <a href="<?= BASE_URL ?>admin/lista-noticias">Listar</a></li>
        </ul>
    </li>
    <hr/>    
    <li><img src="<?= BASE_URL ?>view/admin/images/galerias.png" /> Galerias
        <ul>
            <li>- <a href="<?= BASE_URL ?>admin/cadastro-galeria">Adicionar</a></li>
            <li>- <a href="<?= BASE_URL ?>admin/lista-galerias">Listar</a></li>
        </ul>
    </li>
    <hr/>    
</ul>