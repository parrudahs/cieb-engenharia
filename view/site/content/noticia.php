<?php 
	$noticia = new Jcms\Core\Controllers\NoticiaController();
	$not = $noticia->showPageNoticia($urls[1]);
	$dia = date('d', strtotime($not['data']));
	$mes = date('m', strtotime($not['data']));
	$ano = date('Y', strtotime($not['data']));
?>
		<div class="gtco-section">
			<div class="gtco-container">
				<div class="row gtco-heading">
					<div class="col-md-12">
						<h2><?= $not['titulo'] ?></h2>
                        <h5>Publicado em <?= $dia ?> de <?= Jcms\Core\Ext\DataHr::mesText($mes) ?> de <?= $ano ?></h5>
                        <center><p><img src="<?= BASE_URL ?>public/uploaded_files/noticias/<?= $not['banner'] ?>" class="img-responsive" alt=""></p></center>
						<?= $not['corpo'] ?>
                    	
                    </div>

				</div>

			</div>

		</div>
		<!-- END  -->