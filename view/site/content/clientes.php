
		<div class="gtco-section">
			<div class="gtco-container">
				<div class="row gtco-heading">

					<h2>Clientes</h2>

					<div class="col-md-12">
					<?php 
						$cliente = new Jcms\Core\Controllers\ClienteController();
						$clientes = $cliente->listaTodosClientes();
						if($cliente->getRowCount() > 0): 
							$i = 0;
							foreach ($clientes as $cliente): 
								if($i == 0)
									echo "<p>";	?>
								<a <?= $cliente['site'] != null ? 'href="'.$cliente['site'].'" target="_blank"' : 'href="#"'; ?> >
			                    	<img src="<?= BASE_URL ?>public/uploaded_files/clientes/<?= $cliente['logo']; ?>" alt="<?= $cliente['nome']; ?>">
			                    </a>
                    <?php 
								if($i == 8):
									echo "</p>";
									$i = -1;
								endif;						
								$i++;
							endforeach; 
						endif; 
					?>		
				</div>
			</div>
		</div>
				</div>

			</div>
		</div>
		<!-- END  -->