<?php 
	$portfolio = new Jcms\Core\Controllers\PortfolioController();
	$foto = new Jcms\Core\Controllers\FotoController();

	$port = $portfolio->showPortfolio($urls[2]);
	//print_r($port);
?>
		<div class="gtco-container">
        				<div class="row">
					<div class="col-md-8 col-md-offset-2 gtco-heading text-center">
						<h2><?= $port['nome'] ?></h2>
                        <h3><?= $port['local'] ?> - <?= $port['estado'] ?> (<?= $port['sigla'] ?>)</h3>
						<p><?= $port['descricao'] ?></p>
					</div>
				</div>
			<div class="row">
				<div class="col-md-12">
					<div class="owl-carousel owl-carousel-fullwidth">
					<?php 
						$fotos = $foto->allFotosPortfolio($port['id']);
						foreach ($fotos as $f) {
					?>
						<div class="item">
							<a href="#">
								<img src="<?= BASE_URL ?>public/uploaded_files/portfolio/1110x-<?= $f['imagem']; ?>" alt="Free Website Template by GetTemplates.co">
								<div class="slider-copy">
									<h2><?= $f['descricao']; ?></h2>
								</div>
							</a>
						</div>
					<?php 
						}
					?>
					</div>
				</div>
			</div>
		</div>
    </div>

                <div class="gtco-section">
                <div class="row">
					<div class="col-md-8 col-md-offset-2 gtco-heading text-center">
						<h2>Conheça Outras Obras Similares</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus placerat enim et urna sagittis, rhoncus euismod erat tincidunt. Donec tincidunt volutpat erat.</p>
					</div>
				</div>
			<div class="gtco-container">

				<div class="row">

					<div class="col-md-12">
						<div class="owl-carousel owl-carousel-carousel">
						<?php $portfolios = $portfolio->listaTodosTrabalhos(true, true);
						if($portfolio->getRowCount() > 0){ 
						foreach ($portfolios as $port) {?>
							<div class="item">
								<div class="gtco-item">
									<a href="<?= BASE_URL ?>portfolio-interno/<?= Jcms\Core\Ext\Content::gerarURL($port['nome']); ?>/<?= $port['id']; ?>">
										<img src="<?= BASE_URL ?>public/uploaded_files/portfolio/350x-<?= $port['imagem']; ?>" alt="<?= $port['nome'] ?>" class="img-responsive">
									</a>
									<h2>
										<a href="<?= BASE_URL ?>portfolio-interno/<?= Jcms\Core\Ext\Content::gerarURL($port['nome']); ?>/<?= $port['id']; ?>">
											<?= $port['nome'] ?></a>
									</h2>
									<p class="role"><?= $port['local'] ?> - <?= $port['estado'] ?> (<?= $port['sigla'] ?>)</p>
								</div>
							</div>
						<?php 
							}
						} 
						?>
						</div>
					</div>
                                    				<div class="row gtco-heading">
					<div class="col-md-7 text-left">
						<h2>Orçamento</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus placerat enim et urna sagittis, rhoncus euismod erat tincidunt. Donec tincidunt volutpat erat.</p>
					</div>
					<div class="col-md-3 col-md-push-2 text-center">
						<p class="mt-md"><a href="<?= BASE_URL ?>contato" class="btn btn-special btn-block">Contato</a></p>
					</div>
				</div>

				</div>
			</div>

		</div>

		<!-- END Work -->

				</div>

			</div>
		</div>
		<!-- END  -->