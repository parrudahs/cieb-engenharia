<?php 
	$destaque = new Jcms\Core\Controllers\DestaqueController();
	$noticia = new Jcms\Core\Controllers\NoticiaController();
	$cliente = new Jcms\Core\Controllers\ClienteController();
	$portfolio = new Jcms\Core\Controllers\PortfolioController();
?>
<div class="gtco-container">
			<div class="row">
				<div class="col-md-12">
					<div class="owl-carousel owl-carousel-fullwidth">
						<?php 
						$destaques = $destaque->listaDestaqueHome(3);
						if($destaque->getRowCount() > 0): 
							foreach ($destaques as $destaque): ?>
							<div class="item">
								<a href="#">
									<img src="<?= BASE_URL ?>public/uploaded_files/destaque/<?= $destaque['banner']; ?>" alt="Free Website Template by GetTemplates.co">
									<div class="slider-copy">
										<h2><?= $destaque['titulo']; ?></h2>
									</div>
								</a>
							</div>
						<?php 
							endforeach; 
						endif; 
						?>
					</div>
				</div>
			</div>
		</div>

		<div class="gtco-section">
			<div class="gtco-container">
				<div class="row">
					<div class="col-md-6 gtco-news">
						<h2>Notícias</h2>
						<ul>
							<?php 
								$noticias = $noticia->listaNoticiasHome(2);
								if($noticia->getRowCount() > 0): 
									foreach ($noticias as $noticia): 
										$dia = date('d', strtotime($noticia['data']));
										$mes = date('m', strtotime($noticia['data']));
							?>
							<li>
								<a href="<?= BASE_URL ?>noticia/<?= $noticia['url']; ?>">
									<span class="post-date"><?=  $dia ?> de <?= Jcms\Core\Ext\DataHr::mesText($mes) ?></span>
									<h3><?= $noticia['titulo']; ?></h3>
									<p><?= $noticia['resumo']; ?></p>
								</a>
							</li>
							<?php 
									endforeach; 
								endif; 
							?>
							
						</ul>
						<p><a href="<?= BASE_URL ?>noticias" class="btn btn-sm btn-special">Mais Notícias</a></p>
					</div>
					<!-- END News -->
					<div class="col-md-5 col-md-push-1 gtco-testimonials">
						<h2>Clientes</h2>
						<?php 
							$clientes = $cliente->listaClientesHome(9);
							$total = $cliente->getRowCount();
							if($total > 0): 
								$i = 0;
								$qtd = 0;
								foreach ($clientes as $cliente): 
									if($i == 0)
										echo "<div>";
						?>
								<a <?= $cliente['site'] != null ? 'href="'.$cliente['site'].'" target="_blank"' : 'href="#"'; ?>>
                            		<img src="<?= BASE_URL ?>public/uploaded_files/clientes/<?= $cliente['logo']; ?>" alt="<?= $cliente['nome']; ?>">
                            	</a>
						<?php 
									if($i == 2):
										echo "</div><br>";
										$i = -1;
									endif;

									$qtd++;

									if($qtd == $total)
										echo "</div><br>";
									$i++;
								endforeach; 
							endif; 
						?>							
				</div>
			</div>
		</div>
		<!-- END  -->

        <div class="gtco-section">
			<div class="gtco-container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 gtco-heading text-center">
						<h2>Portfólio</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus placerat enim et urna sagittis, rhoncus euismod erat tincidunt. Donec tincidunt volutpat erat.</p>
					</div>
				</div>
				<div class="row">

					<div class="col-md-12">
						<div class="owl-carousel owl-carousel-carousel">
						<?php $portfolios = $portfolio->listaTodosTrabalhos(true, true);
						if($portfolio->getRowCount() > 0){ 
						foreach ($portfolios as $port) {?>
							<div class="item">
								<div class="gtco-item">
									<a href="<?= BASE_URL ?>portfolio-interno/<?= Jcms\Core\Ext\Content::gerarURL($port['nome']); ?>/<?= $port['id']; ?>">
										<img src="<?= BASE_URL ?>public/uploaded_files/portfolio/350x-<?= $port['imagem']; ?>" alt="<?= $port['nome'] ?>" class="img-responsive">
									</a>
									<h2>
										<a href="<?= BASE_URL ?>portfolio-interno/<?= Jcms\Core\Ext\Content::gerarURL($port['nome']); ?>/<?= $port['id']; ?>">
											<?= $port['nome'] ?></a>
									</h2>
									<p class="role"><?= $port['local'] ?> - <?= $port['estado'] ?> (<?= $port['sigla'] ?>)</p>
								</div>
							</div>
						<?php 
							}
						} 
						?>
						</div>
					</div>

				</div>
			</div>
		</div>
		<!-- END Work -->