<?php 
	$institucional = new Jcms\Core\Controllers\InstitucionalController();
	$inst = $institucional->showPageInstitucional();

?>
		<div class="gtco-section">
			<div class="gtco-container">
				<div class="row gtco-heading">
					<div class="col-md-12">
						<h2><?= $inst['titulo'] ?></h2>
                        <center><p><img src="<?= BASE_URL ?>public/uploaded_files/institucional/<?= $inst['banner']; ?>" class="img-responsive" alt=""></p></center>
						<?= $inst['corpo'] ?>
					</div>

				</div>

			</div>
		</div>
				</div>

			</div>
		</div>
		<!-- END  -->