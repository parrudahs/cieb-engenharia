	<div class="gtco-section">
			<div class="gtco-container">
				<div class="row gtco-heading">
					<div class="col-md-12">
						<h2>Contato</h2>
						<p>Envie-nos sua opinião, agende uma visita e torne-se mais um cliente de sucesso com a Cieb Engenharia</p>
					</div>

				</div>
				<div class="row">
					<div class="col-md-6">
						<?php if (isset($_SESSION['output_message'])) { ?>
							<div class='alert alert-<?= $_SESSION['output_message_tipo'] ?> alert-dismissable'>
								<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
								<strong class='msgError'><?= $_SESSION['output_message'] ?></strong>
							</div>
							<?php unset($_SESSION['output_message']);
						} ?>
						<form action="<?= BASE_URL ?>action/contato/envio-email" method="post">
							<div class="form-group">
								<label for="name">Nome</label>
								<input type="text" class="form-control" id="name" name="nome">
							</div>
							<div class="form-group">
								<label for="name">E-mail</label>
								<input type="text" class="form-control" id="email" name="email">
							</div>
							<div class="form-group">
								<label for="message"></label>
								<textarea name="mensagem" id="message" cols="30" rows="10" class="form-control"></textarea>
							</div>
							<div class="form-group">
								<input type="submit" class="btn btn btn-special" value="Enviar">
							</div>
						</form>
					</div>
					<div class="col-md-5 col-md-push-1">
						<div class="gtco-contact-info">
							<h3>Endereço</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam non arcu vel quam malesuada varius venenatis nec lacus.</p>
							<ul >
								<li class="address">Rua Tupinambás Qd. 6 Lt. 10 - Vila Brasília - Aparecida de Goiânia / Goiás</li>
								<li class="phone"><a href="tel://1234567890">(62) 3097-2342 / (62) 3097-2343</a></li>
								<li class="email"><a href="#">contato@ciebeng.com.br</a></li>
								<li class="url"><a href="#">facebook.com/ciebengenharia/</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END Contact -->

		<div><iframe src="https://www.google.com/maps/d/embed?mid=167V1PnfD5noiehow-ym-Y4BSIJw&hl=pt-PT" width="1400" height="400"></iframe></div>
