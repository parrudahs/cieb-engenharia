 		<div class="gtco-section">
			<div class="gtco-container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 gtco-heading text-center">
						<h2>Portfólio</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus placerat enim et urna sagittis, rhoncus euismod erat tincidunt. Donec tincidunt volutpat erat.</p>
					</div>
				</div>
				<div class="row tab">
					<ul class="nav nav-tabs" >
					<?php 
						$portfolio = new Jcms\Core\Controllers\PortfolioController(); 
						$tags = $portfolio->getTags();
						$i = 0;
						foreach ($tags as $t) {
					?>
					  <li <?= $i == 0 ? "class='active'" : " " ?>><a data-toggle="tab" class="btn-special " href="#<?= $t['id'] ?>"><?= $t['nome'] ?></a></li>

					<?php 
							$i++; 
						}
						$i = 0; 
					?>
					</ul>
				</div>
				<div class="row">
					<div class="tab-content">
						<?php foreach ($tags as $tag) { ?>
					  	<div id="<?= $tag['id'] ?>" class="tab-pane fade in <?= $i == 0 ? "active" : " " ?>">
					  		<?php 
					  		$portfolios = $portfolio->listaTodosTrabalhos(true, true, $tag['id']);
					  		if($portfolio->getRowCount() > 0) {
					  			foreach ($portfolios as $port) { 
					  		?>
						    <div class="col-md-4 col-sm-6 col-xs-12">
								<div class="item">
									<div class="gtco-item">
										<a href="<?= BASE_URL ?>portfolio-interno/<?= Jcms\Core\Ext\Content::gerarURL($port['nome']); ?>/<?= $port['id']; ?>">
											<img src="<?= BASE_URL ?>public/uploaded_files/portfolio/350x-<?= $port['imagem']; ?>" alt="<?= $port['nome'] ?>" class="img-responsive">
										</a>
										<h2>
											<a href="<?= BASE_URL ?>portfolio-interno/<?= Jcms\Core\Ext\Content::gerarURL($port['nome']); ?>/<?= $port['id']; ?>">
											<?= $port['nome'] ?>
											</a>
										</h2>

										<p class="role"><?= $port['local'] ?> - <?= $port['estado'] ?> (<?= $port['sigla'] ?>)</p>
									</div>
								</div>
							</div>
							<div class="clearfix visible-sm-block"></div>
							<?php 	} 

								} else 
									echo "<p>No momento não temos nenhum trabalho cadastrado nesta categoria!!</p>";
							?>
					  	</div>
					  	<?php $i++; } ?>
				</div>
			</div>
		</div>

		<!-- END  -->