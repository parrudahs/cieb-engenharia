<?php 
	$noticia = new Jcms\Core\Controllers\NoticiaController();
	$qtdPorPagina = 3;
	$pagina = (isset($_GET['page'])) ? $_GET['page'] : 1;
	$noticias = $noticia->listaNoticias($pagina, $qtdPorPagina);
?>
		<div class="gtco-section">
			<div class="gtco-container">
				<div class="row">
					<div class="col-md-12 gtco-news">
						<h2>Notícias</h2>
						<ul>
						<?php 
							foreach ($noticias as $not) { 
								$dia = date('d', strtotime($not['data']));
								$mes = date('m', strtotime($not['data']));
						?>
							<li>
								<a href="<?= BASE_URL ?>noticia/<?= $not['url']; ?>">
									<span class="post-date"><?= $dia ?> de <?= Jcms\Core\Ext\DataHr::mesText($mes) ?></span>
									<h3><?= $not['titulo'] ?></h3>
									<p><?= $not['resumo'] ?></p>
								</a>
							</li>
						<?php } ?>

						</ul>
						<p>
						<?php 
							$paginar = new Jcms\Core\Ext\Paginacao();
							
							$noticia->listaNoticiasHome();

							$paginar->paginar(BASE_URL.'noticias', $pagina, $noticia->getRowCount(), $qtdPorPagina, 2);

							echo $paginar->getPaginacao();
						?>
						</p>
						<!--<p><a href="#" class="btn btn-sm btn-special">1</a> <a href="#" class="btn btn-sm btn-special">2</a> <a href="#" class="btn btn-sm btn-special">3</a></p>-->
					</div>
					<!-- END News -->

				</div>
			</div>
		</div>
		<!-- END  -->