<!DOCTYPE HTML>
<!--
	Aesthetic by gettemplates.co
	Twitter: http://twitter.com/gettemplateco
	URL: http://gettemplates.co
-->
<html>
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Cieb Engenharia</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Free HTML5 Website Template by gettemplates.co" />
	<meta name="keywords" content="free website templates, free html5, free template, free bootstrap, free website template, html5, css3, mobile first, responsive" />
	<meta name="author" content="gettemplates.co" />

  	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<link href="https://fonts.googleapis.com/css?family=Merriweather:300,400|Montserrat:400,700" rel="stylesheet">
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="<?= BASE_URL ?>css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="<?= BASE_URL ?>css/icomoon.css">
	<!-- Themify Icons-->
	<link rel="stylesheet" href="<?= BASE_URL ?>css/themify-icons.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="<?= BASE_URL ?>css/bootstrap.css">

	<!-- Owl Carousel  -->
	<link rel="stylesheet" href="<?= BASE_URL ?>css/owl.carousel.min.css">
	<link rel="stylesheet" href="<?= BASE_URL ?>css/owl.theme.default.min.css">

	<!-- Theme style  -->
	<link rel="stylesheet" href="<?= BASE_URL ?>css/style.css">

	<!-- Modernizr JS -->
	<script src="<?= BASE_URL ?>js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->
	</head>

	<body>
		
	<div class="gtco-loader"></div>
	
	<div id="page">

		<nav class="gtco-nav" role="navigation">
			<div class="gtco-container">
				
				<div class="row">
					<div class="col-sm-2 col-xs-12">
						<div id="gtco-logo"><a href="<?= BASE_URL ?>"><img src="<?= BASE_URL ?>images/logo.png" width="150" height="74" alt=""></div>
					</div>
					<div class="col-xs-10 text-right menu-1">
						<ul>
							<li <?= $urls[0] == null ? "class='active'" : " " ?> >
								<a href="<?= BASE_URL ?>">Home</a>
							</li>
							<li <?= $urls[0] == 'institucional' ? "class='active'" : " " ?> >
								<a href="<?= BASE_URL ?>institucional">Institucional</a>
							</li>
							<li <?= $urls[0] == 'servico' ? "class='active has-dropdown'" : "class='has-dropdown'" ?> >
								<a href="#">Serviços</a>
								<ul class="dropdown">
									<?php 
										$servico = new Jcms\Core\Controllers\ServicoController();
										foreach ($servico->listaTodosServicos() as $serv) {?>
									<li>
										<a href="<?= BASE_URL ?>servico/<?= $serv['url'] ?>"><?= $serv['titulo'] ?></a>
									</li>
									<?php } ?>
								</ul>
							</li>
                            <li <?= $urls[0] == 'clientes' ? "class='active'" : " " ?>>
                            	<a href="<?= BASE_URL ?>clientes">Clientes</a>
                            </li>
                            <li <?= $urls[0] == 'galeria-portfolio' || $urls[0] == 'portfolio-interno' ? "class='active'" : " " ?>>
                            	<a href="<?= BASE_URL ?>galeria-portfolio">Portfólio</a>
                            </li>
							<li <?= $urls[0] == 'contato' ? "class='active'" : " " ?>>
								<a href="<?= BASE_URL ?>contato">Contato</a>
							</li>
						</ul>
					</div>
				</div>
				
			</div>
		</nav>

		<?php
            include("content/".$requested_file);
        ?>

		<footer id="gtco-footer" class="gtco-section" role="contentinfo">
			<div class="gtco-container">
				<div class="row row-pb-md">
					<div class="col-md-4 gtco-widget gtco-footer-paragraph">
						<img src="<?= BASE_URL ?>images/logogrey.png" width="203" height="100" alt="">
					</div>
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-6 gtco-footer-link">
								<h3>Menu</h3>
								<ul class="gtco-list-link">
									<li><a href="<?= BASE_URL ?>">Home</a></li>
									<li><a href="<?= BASE_URL ?>institucional">Institucional</a></li>
									<li><a href="<?= BASE_URL ?>clientes">Clientes</a></li>
									<li><a href="<?= BASE_URL ?>galeria-portfolio">Portfólio</a></li>
									<li><a href="<?= BASE_URL ?>contato">Contato</a></li>
								</ul>
							</div>
							<div class="col-md-6 gtco-footer-link">
								<h3>Serviços</h3>
								<ul class="gtco-list-link">
									<?php foreach ($servico->listaTodosServicos() as $serv) { ?>
									<li>
										<a href="<?= BASE_URL ?>servico/<?= $serv['url'] ?>"><?= $serv['titulo'] ?></a>
									</li>
									<?php } ?>
								</ul>
							</div>
						</div>
					</div>

				</div>
			</div>
			<div class="gtco-copyright">
				<div class="gtco-container">
					<div class="row">
						<div class="col-md-6 text-left">
							<p><small>&copy; 2017 Cieb Engenharia - Todos os direitos reservados </small></p>
						</div>
						<div class="col-md-6 text-right">
							<p><small></small> </p>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<!-- END footer -->

	</div>

	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
	</div>
	
	<!-- jQuery -->
	<script src="<?= BASE_URL ?>js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="<?= BASE_URL ?>js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="<?= BASE_URL ?>js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="<?= BASE_URL ?>js/jquery.waypoints.min.js"></script>
	<!-- Carousel -->
	<script src="<?= BASE_URL ?>js/owl.carousel.min.js"></script>

	<!-- Main -->
	<script src="<?= BASE_URL ?>js/main.js"></script>

	</body>
</html>

